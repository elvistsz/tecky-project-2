import express from 'express';

export const isLoggedInAPI = (req:express.Request,res:express.Response,next:express.NextFunction)=>{
    if(req.session && req.session.user){
        next();
    }else{
        res.status(401).json({success:false});
    }
}

export const isLoggedIn = (req:express.Request,res:express.Response,next:express.NextFunction)=>{
    if(req.session && req.session.user){
        next();
    } else {
        res.status(401).redirect('/');
    }
}
