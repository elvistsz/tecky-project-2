export interface User{
    id:number
    username:string
    password:string
    friend_list:number
    profile_pic:string
    email:string
}

export interface VoiceMessage{
    id:number
    users_id:number
    message:string
    hidden:boolean
}

export interface GoogleUser{
    name: string
    email: string
}