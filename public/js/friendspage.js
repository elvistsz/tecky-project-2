

// ------ google sign in form ------ //

const getCurrentUser = async () => {
    const res = await fetch('/current-user');
    return await res.json();
}




//-------------------------Friend List-------------------------

const friendList = document.querySelector('.friends')
const friendContainer = document.querySelector('.friend-container')
const content = document.querySelector('.content')
const jumpIn = document.querySelector('.jump-in')

const loadFriendList = async function () {
    friendList.innerHTML = ""
    const users = await fetch('/allUsers')
    await loadPendingFriend();
    await loadCurrentFriend();
    await loadBlockFriend();
    const friends = document.querySelectorAll('.friend')

    for (let friend of friends) {
        friend.addEventListener('click', async () => {
            // console.log(`fetching id ${friend.getAttribute('data-id')}`)
            let res = await fetch(`/profile/${friend.getAttribute('data-id')}`)
            let user = await res.json();
            // console.log("user", user)

            res = await fetch(`/relationship/${user.id}`)
            let relation = (await res.json())[0];

            // console.log("relation", relation)
            //	0 Pending
            //  1 Accepted
            //  2 Declined
            //  3 Blocked

            //set condition and switch case

            jumpIn.innerHTML = `
                
                <div class="returnBtn"><i class="fas fa-undo-alt"></i>BACK</div>   
                <div class="row profile" style="position:relative" data-id="${user.id}">
                <div class="col-sm-12 personProfile">
                
                    <!----------------- Profile Pic ------------>
                    <div class="container-fluid profile-Container">
                        <img class="profilePic" src="../avatar/${user.profile_pic}.png">
                    </div>  
                    <div class="profile-text">
                        <div class="profile-info">
                            <div class="profile-name">
                                ${user.nickname}
                            </div>
                            <div class="six-buttons">
                                <div class="set1">            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`

            const buttons = document.querySelector('.set1')
            // console.log("relation.action_users_id", relation.action_users_id)
            if (relation.status === 0) {
                if (relation.action_users_id === user.id) {
                    // console.log('pending status, should show accept and decline btns')
                    buttons.innerHTML = `
                    <div class="accept" data-id="${user.id}"><i class="fas fa-plus"></i>Accept friend request</div>
                    <div class="reject" data-id="${user.id}"><i class="fas fa-minus"></i>Decline friend request</div>
                    `
                } else {
                    buttons.innerHTML = `
                    <div class="pending" data-id="${user.id}">pending</div>
                    `
                }
            } else if (relation.status === 1) {
                // console.log('accepted status, should show block and unfriend btns')
                buttons.innerHTML = `   
                <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Block User</div>
                <div class="rmFrd" data-id="${user.id}"><i class="fas fa-minus"></i>Remove friend</div>
                `
            } else if (relation.status === 2) {
                // console.log('decline status, should show fd request btn')
                buttons.innerHTML = `    
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            } else if (relation.status === 3) {
                // console.log('blocked status, should show unblock btn')
                if (relation.action_users_id !== user.id) {
                    buttons.innerHTML = `   
                <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Unblock User</div>
                `
                }
            } else {
                // console.log('not relationship before, should show fd request btn')
                buttons.innerHTML = `
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            }
            // <!---------------END OF Profile Pic ------------>
            res = await fetch(`/voiceMessage/user/${user.id}`)
            let messages = await res.json()



            // console.log("message", messages)
            for (let message of messages) {

                const likeAndDislikeCount = await fetch(`/likeCount/${message.id}`);
                const likeCountResult = await likeAndDislikeCount.json()

                const res = await fetch(`/likeOrDislike/${message.id}`)
                interactionStatus = await res.json();

                jumpIn.innerHTML += `    
                <div class="col-sm-12 posts profile">
                <div class="row">
                    <img class="post-profilePic" src="../avatar/${user.profile_pic}.png">
                    <div class="post-text">
                        <div class="post-person-name">
                            ${user.nickname}
                        </div>
                        <div class="post-time">
                        123213${(new Date(message.updated_at)).toDateString()} ${(new Date(message.updated_at)).toLocaleTimeString()}
                        </div>
                    </div>
                </div>
                <div class="row post-padding">
                <div class="row post-inner-content"">
                <div class="post-content">
                <img class="emoji" src="../emoji/${message.emoji}.png">
                            <div class="audioWave">
                                <audio controls>
                                    <source src="../uploads/${message.message}"
                                            type="audio/mpeg">
                                </audio>
                            </div>
                            <div class="col-sm-12 interaction LDC-button" id= "interaction-${message.id}">
                                <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                                <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                                <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                                <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                                <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                                <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`

                if (interactionStatus[0]) {
                    if (interactionStatus[0].like_or_dislike) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like green" data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                    } else if (interactionStatus[0].like_or_dislike === false) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike red" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                    } else if (interactionStatus[0].like_or_dislike === null) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                    }
                }
            }
            await enableInteraction();
            await messageDeleteButton();
            await relationshipBtns();
            let returnBtn = document.querySelector('.returnBtn')
            returnBtn.addEventListener('click', () => {
                document.location.href = '/html/home.html'
            })
        })
    }

    $(document).on('click', '.returnBtn', async () => {
        // console.log('1234')
        // document.location.href = '/html/home.html'
        location.reload();
    })
}

loadFriendList();


const loadPendingFriend = async () => {
    const res = await fetch('/pendingFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend pending" data-id="${friend.friendId}">${friend.friendUsername}</div>`
        // console.log("displaying Pending Friend list")
    }
}

const loadCurrentFriend = async () => {
    const res = await fetch('/currentFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend current" data-id="${friend.friendId}">${friend.friendUsername}</div>`
        // console.log("displaying Current Friend list")
    }
}

const loadBlockFriend = async () => {
    const res = await fetch('/blockFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend block" data-id="${friend.friendId}">${friend.friendUsername}</div>`
        // console.log("displaying Blocked Friend list")
    }
}

const loadView = async () => {
    //-------------search bar and toggle-------------
    jumpIn.innerHTML = `<div class="row" style="min-width: 100%;">
    <div class="col-sm-12 s-container">
        <div class="search-bar">
            <input type="text" autocomplete="off" spellcheck="false" placeholder="Search..." id="searchBar">
            <div class="search">
                <i class="fas fa-search"></i>
            </div>
       </div>
       
       <div class="s-area">search area</div>
       <div class="switch">
            <div class="box">
                <div>GLOBAL</div>
                <div>WITHIN FRIENDS</div>
            </div>
        </div>
    </div>
</div>
    <div class="searchResult"> </div>`
    await loadViewUsers()
    await loadViewAndAdd();
    let searchFrom = document.querySelector('.switch')

    // console.log("searchFrom", searchFrom)
    // console.log("adding event listener")
    searchFrom.addEventListener('click', async () => {
        if (searchFrom.classList.contains('active')) {
            await loadViewUsers();
            await loadViewAndAdd();
            // console.log('active')
        } else {
            // console.log('not active')
            await loadViewFriends();
            await loadViewAndDelete();
        }
    })
}

const loadViewUsers = async () => {
    // console.log("loading users list")

    let res = await fetch(`/global`)
    const users = await res.json();
    let searchResults = ``
    let searchResult = document.querySelector('.searchResult')
    searchResult.innerHTML = ``
    for (let user of users) {

        res = await fetch(`/user/${user.id}`)
        let relation = await res.json();

        //set logic for ban user
        // normal fds
        searchResults += `
        <div class="person nameCard profile" data-id="${user.id}">
        <div class="container-fluid picContainer" style="max-width: 135px">
            <img class="profilePic" src="../avatar/${user.profile_pic}.png">
        </div>  
        <div class="person-text">
            <div class="person-info">
                <div class="person-name">
                    ${user.nickname}
                </div>
            </div>
            <div class="action" data-Userid="${user.id}">
                <div data-id="${user.id}" class="viewButton">VIEW</div>
            </div>
        </div>
        </div>
        `
    }
    searchResult.innerHTML = searchResults;
    await getButtons();
}

const getButtons = async () => {
    const actions = document.querySelectorAll('.action')
    for (let action of actions) {
        let id = action.getAttribute('data-userid')


        let res = await fetch(`/relationship/${id}`)
        let relation = await res.json();
        // console.log("relation", relation2)

        if (relation[0]) {
            if (relation[0].status === 0) {
                // console.log('pending status, should show accept and decline btns')
                action.innerHTML += `
                    <div data-id="${id}" class="viewButton">Pending</div>
                    `
            } else if (relation[0].status === 1) {
                // console.log('accepted status, should show block and unfriend btns')
                action.innerHTML += `   
                    <div data-id="${id}" class="viewButton">Delete</div>
                    `
            } else if (relation[0].status === 2) {
                // console.log('decline status, should show fd request btn')
                action.innerHTML += `    
                    <div data-id="${id}" class="viewButton">Add</div>
                    `
            } else if (relation[0].status === 3) {
                // console.log('iddddddddddfdudhwjwroijroiwjer', id)
                // console.log(relation[0].action_users_id)
                if (relation[0].action_users_id !== parseInt(id)) {
                    // console.log('blocking status, should show unblock btn')
                    action.innerHTML += `   
                        <div data-id="${id}" class="viewButton">Unblock</div>
                        `
                } else {
                    // console.log('block status, should show unblock btn')
                    action.innerHTML += `   
                        <div data-id="${id}" class="viewButton">Blocked</div>
                        `
                }
            }
        } else {
            // console.log('not relationship before, should show fd request btn')
            action.innerHTML += `
                <div data-id="${id}" class="viewButton">ADD</div>
                `
        }

    }
}

const loadViewFriends = async () => {
    const currentUser = getCurrentUser();

    let res = await fetch(`/friend`)
    const users = await res.json();


    let searchResult = document.querySelector('.searchResult')
    searchResult.innerHTML = ``
    //-------------view fd list-------------
    for (let user of users) {
        //set logic for ban user
        // normal fds
        searchResult.innerHTML += `
        <div class="person nameCard profile" data-id="${user.id}">
        <div class="container-fluid picContainer" style="max-width: 135px">
            <img class="profilePic" src="../avatar/${user.profile_pic}.png">
        </div>  
        <div class="person-text">
            <div class="person-info">
                <div class="person-name">
                    ${user.nickname}
                </div>
            </div>
            <div class="action" data-userid="${user.id}">
                <div data-id="${user.id}" class="viewButton">VIEW</div>
            </div>
        </div>
    </div>
    `
    }
    await getButtons();

}

// async function main() {
//     await loadFriendList();
// }
// main()

//Buttons show in profile and relationship function 

const add = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Request sent')
    document.location.href = '/html/friendspage.html'
    // console.log(result)
}

const unfriend = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('unfriended')
    document.location.href = '/html/friendspage.html'
    // console.log(result)
}

const blockOrUnBlock = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    if (result.message === "User Blocked") {
        alert('User Blocked')
        document.location.href = '/html/friendspage.html'
    } else {
        alert('User unBlocked')
        document.location.href = '/html/friendspage.html'
    }
    // console.log(result)
}

const accept = async (id) => {
    const res = await fetch(`/accept/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Accepted')
    document.location.href = '/html/friendspage.html'
    // console.log(result)
}

const decline = async (id) => {
    const res = await fetch(`/decline/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Declined')
    document.location.href = '/html/friendspage.html'
    // console.log(result)
}

const relationshipBtns = async () => {

    let addFrdBtns = document.querySelectorAll('.addFrd')
    let acceptBtns = document.querySelectorAll('.accept')
    let rejectBtns = document.querySelectorAll('.reject')
    let blockBtns = document.querySelectorAll('.block')
    let rmFrdBtns = document.querySelectorAll('.rmFrd')

    for (let addFrdBtn of addFrdBtns) {
        addFrdBtn.addEventListener('click', () => {
            // console.log("clicked add fd")
            let id = addFrdBtn.getAttribute('data-id')
            add(id);
        })
    }
    for (let acceptBtn of acceptBtns) {
        acceptBtn.addEventListener('click', () => {
            // console.log("clicked accept")
            let id = acceptBtn.getAttribute('data-id')
            // console.log("iddddddd", id)
            accept(id);
        })
    }
    for (let rejectBtn of rejectBtns) {
        rejectBtn.addEventListener('click', () => {
            // console.log("clicked reject")
            let id = rejectBtn.getAttribute('data-id')
            decline(id);
        })
    }
    for (let blockBtn of blockBtns) {
        blockBtn.addEventListener('click', () => {
            // console.log("clicked block")
            let id = blockBtn.getAttribute('data-id')
            blockOrUnBlock(id);
        })
    }
    for (let rmFrdBtn of rmFrdBtns) {

        rmFrdBtn.addEventListener('click', () => {
            // console.log("clicked rmFrdBtn")
            let id = rmFrdBtn.getAttribute('data-id')
            unfriend(id);
        })
    }
}


const loadViewAndAdd = async () => {
    let viewButtons = document.querySelectorAll('.viewButton')

    for (let viewButton of viewButtons) {
        viewButton.addEventListener('click', async () => {
            let res = await fetch(`/profile/${viewButton.getAttribute('data-id')}`)
            let user = await res.json();


            res = await fetch(`/relationship/${user.id}`)
            let relation = (await res.json())[0];

            //set condition and switch case
            jumpIn.innerHTML = `
                <div class="row profile" style="position:relative" data-id="${user.id}">
                <div class="col-sm-12 personProfile">
                <div class="returnBtn"><i class="fas fa-undo-alt"></i>BACK</div>   
                    <!----------------- Profile Pic ------------>
                    <div class="container-fluid profile-Container">
                        <img class="profilePic" src="../avatar/${user.profile_pic}.png">
                    </div>  
                    <div class="profile-text">
                        <div class="profile-info">
                            <div class="profile-name">
                                ${user.nickname}
                            </div>
                            <div class="six-buttons">
                                <div class="set1">            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`
            const buttons = document.querySelector('.set1')
            if (relation) {
                if (relation.status === 0) {
                    if (relation.action_users_id === user.id) {
                        // console.log('pending status, should show accept and decline btns')
                        buttons.innerHTML = `
                        <div class="accept" data-id="${user.id}"><i class="fas fa-plus"></i>Accept friend request</div>
                        <div class="reject" data-id="${user.id}"><i class="fas fa-minus"></i>Decline friend request</div>
                        `
                    } else {
                        buttons.innerHTML = `
                        <div class="pending" data-id="${user.id}">pending</div>
                        `
                    }
                } else if (relation.status === 1) {
                    // console.log('accepted status, should show block and unfriend btns')
                    buttons.innerHTML = `   
                    <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Block User</div>
                    <div class="rmFrd" data-id="${user.id}"><i class="fas fa-minus"></i>Remove friend</div>
                    `
                } else if (relation.status === 2) {
                    // console.log('decline status, should show fd request btn')
                    buttons.innerHTML = `    
                    <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                    `
                } else if (relation.status === 3) {
                    if (relation.action_users_id !== user.id) {
                        buttons.innerHTML = `   
                    <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Unblock User</div>
                    `
                    }
                }
            } else {
                // console.log('not relationship before, should show fd request btn')
                buttons.innerHTML = `
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            }
            // console.log("user.id", user.id)
            // <!---------------END OF Profile Pic ------------>
            res = await fetch(`/voiceMessage/user/${user.id}`)
            let messages = await res.json()

            // console.log("message", messages)
            for (let message of messages) {

                const likeAndDislikeCount = await fetch(`/likeCount/${message.id}`);
                const likeCountResult = await likeAndDislikeCount.json()

                const res = await fetch(`/likeOrDislike/${message.id}`)
                const interactionStatus = await res.json();

                jumpIn.innerHTML += `    <div class="col-sm-12 posts profile">
                <div class="row">
                    <img class="post-profilePic" src="../avatar/${user.profile_pic}.png">
                    <div class="post-text">
                        <div class="post-person-name">
                            ${user.nickname}
                        </div>
                        <div class="post-time">
                            ${(new Date(message.updated_at)).toDateString()} ${(new Date(message.updated_at)).toLocaleTimeString()}
                        </div>
                    </div>
                </div>
                <div class="row post-padding">
                <div class="row post-inner-content"">
                <div class="post-content">
                <img class="emoji" src="../emoji/${message.emoji}.png">
                        <div class="audioWave">
                        <audio controls>
                                        <source src="../uploads/${message.message}"
                                            type="audio/mpeg">
                                    </audio>
                        </div>
                        <div class="col-sm-12 interaction LDC-button" id= "interaction-${message.id}">
                            <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                            <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                            <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                            <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                            <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                            <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                        </div>
                    </div>
                </div>
                </div>
                </div>`

                if (interactionStatus[0]) {
                    if (interactionStatus[0].like_or_dislike) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like green" data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    </div>
                    `
                    } else if (interactionStatus[0].like_or_dislike === false) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike red" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                </div>
                    `
                    } else if (interactionStatus[0].like_or_dislike === null) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                    }
                }
                await enableInteraction();
                await messageDeleteButton();
            }
            await relationshipBtns();
        })
    }


}

const loadViewAndDelete = async () => {
    let viewButtons = document.querySelectorAll('.viewButton')
    for (let viewButton of viewButtons) {
        viewButton.addEventListener('click', async () => {
            let res = await fetch(`/profile/${viewButton.getAttribute('data-id')}`)
            let user = await res.json();
            // console.log("user", user)

            res = await fetch(`/relationship/${user.id}`)
            let relation = (await res.json())[0];

            // console.log("relation", relation)
            //	0 Pending
            //  1 Accepted
            //  2 Declined
            //  3 Blocked

            //set condition and switch case

            jumpIn.innerHTML = `
                <div class="row profile" style="position:relative" data-id="${user.id}">
                <div class="col-sm-12 personProfile">
                <div class="returnBtn"><i class="fas fa-undo-alt"></i>BACK</div>   
                    <!----------------- Profile Pic ------------>
                    <div class="container-fluid profile-Container">
                        <img class="profilePic" src="../avatar/${user.profile_pic}.png">
                    </div>  
                    <div class="profile-text">
                        <div class="profile-info">
                            <div class="profile-name">
                                ${user.nickname}
                            </div>
                            <div class="six-buttons">
                                <div class="set1">            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`
            const buttons = document.querySelector('.set1')
            if (relation.status === 0) {
                if (relation.action_users_id === user.id) {
                    // console.log('pending status, should show accept and decline btns')
                    buttons.innerHTML = `
                    <div class="accept" data-id="${user.id}"><i class="fas fa-plus"></i>Accept friend request</div>
                    <div class="reject" data-id="${user.id}"><i class="fas fa-minus"></i>Decline friend request</div>
                    `
                } else {
                    buttons.innerHTML = `
                    <div class="pending" data-id="${user.id}">pending</div>
                    `
                }
            } else if (relation.status === 1) {
                // console.log('accepted status, should show block and unfriend btns')
                buttons.innerHTML = `   
                <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Block User</div>
                <div class="rmFrd" data-id="${user.id}"><i class="fas fa-minus"></i>Remove friend</div>
                `
            } else if (relation.status === 2) {
                // console.log('decline status, should show fd request btn')
                buttons.innerHTML = `    
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            } else if (relation.status === 3) {
                if (relation.action_users_id !== user.id) {
                    buttons.innerHTML = `   
                <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Unblock User</div>
                `
                }
            } else {
                // console.log('not relationship before, should show fd request btn')
                buttons.innerHTML = `
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            }
            // console.log("user.id", user.id)
            // <!---------------END OF Profile Pic ------------>
            res = await fetch(`/voiceMessage/user/${user.id}`)
            let messages = await res.json()

            // console.log("message", messages)
            for (let message of messages) {

                const likeAndDislikeCount = await fetch(`/likeCount/${message.id}`);
                const likeCountResult = await likeAndDislikeCount.json()

                const res = await fetch(`/likeOrDislike/${message.id}`)
                const interactionStatus = await res.json();


                jumpIn.innerHTML += `    <div class="col-sm-12 posts profile">
                <div class="row">
                    <img class="post-profilePic" src="../avatar/${user.profile_pic}.png">
                    <div class="post-text">
                        <div class="post-person-name">
                            ${user.nickname}
                        </div>
                        <div class="post-time">
                            ${(new Date(message.updated_at)).toDateString()} ${(new Date(message.updated_at)).toLocaleTimeString()}
                        </div>
                    </div>
                </div>
                <div class="row post-padding">
                <div class="post-content">
                <img class="emoji" src="../emoji/${message.emoji}.png">
                        <div class="audioWave">
                        <audio controls>
                                        <source src="../uploads/${message.message}"
                                            type="audio/mpeg">
                                    </audio>
                        </div>
                        <div class="col-sm-12 interaction LDC-button" id= "interaction-${message.id}">
                        <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                        <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                        <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                        <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                        <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                        <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                        </div>
                    </div>
                </div>
            </div>`
                if (interactionStatus[0]) {
                    if (interactionStatus[0].like_or_dislike) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                </div>
                `
                    } else if (interactionStatus[0].like_or_dislike === false) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
            </div>
                `
                    } else if (interactionStatus[0].like_or_dislike === null) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                `
                    }
                }
            } await enableInteraction();
            // await relationshipBtns();
        })
    }

    await messageDeleteButton();
    await relationshipBtns();

}

// ------- add class active for .friend ------ //
$(document).on('click', '.friend', function () {
    if ($('.friend').hasClass('active')) {
        $('.friend').removeClass('active')
        $(this).addClass('active');
    } else {
        $(this).addClass('active');
    }
})

// ------ remove class active for .friend when click buttons within navbar ------ //
$(document).on('click', '.button', function () {
    if ($('.friend').hasClass('active')) {
        $('.friend').removeClass('active')
    }
})

// ------ top sticky navbar --- //
window.onscroll = function () {
    if (window.pageYOffset > 45) {
        $('.topBar').addClass('flip')
    } else {
        $('.topBar').removeClass('flip')
    }

}

$(document).on('click', '.switch', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})

// searchBar


async function search() {
    let searchbar = document.querySelector('#searchBar')
    let searchFrom = document.querySelector('.switch')
    searchbar.addEventListener('keydown', async function (event) {
        if (event.keyCode === 13 && searchbar.value !== "") {
            // console.log(searchbar.value)
            if (searchFrom.classList.contains('active')) {
                // console.log("loadingViewSearchUsers")
                await loadViewSearchFriends(searchbar.value);
                // await loadViewAndDelete();
                await loadViewAndAdd();
                // console.log('active')
            } else {
                await loadViewSearchUsers(searchbar.value);
                await loadViewAndAdd();
                // console.log('not active')
            }

        }
    })
}

const loadViewSearchUsers = async (content) => {
    // console.log("loading users list")

    let res = await fetch(`/users/global/${content}`)
    const users = await res.json();
    // console.log("users", users)

    let searchResult = document.querySelector('.searchResult')
    searchResult.innerHTML = ``
    for (let user of users) {

        res = await fetch(`/user/${user.id}`)
        let relation = await res.json();

        //set logic for ban user
        // normal fds
        searchResult.innerHTML += `
        <div class="person nameCard profile" data-id="${user.id}">
        <div class="container-fluid picContainer" style="max-width: 135px">
            <img class="profilePic" src="../avatar/${user.profile_pic}.png">
        </div>  
        <div class="person-text">
            <div class="person-info">
                <div class="person-name">
                    ${user.nickname}
                </div>
            </div>
            <div class="action" data-userid="${user.id}">
                <div data-id="${user.id}" class="viewButton">VIEW</div>
            </div>
        </div>
        </div>
        `
    }
    await getButtons();

}

const loadViewSearchFriends = async (content) => {
    // console.log("loading users list")

    let res = await fetch(`/users/friends/${content}`)
    const users = await res.json();

    let searchResult = document.querySelector('.searchResult')
    searchResult.innerHTML = ``
    for (let user of users) {

        res = await fetch(`/user/${user.id}`)
        let relation = await res.json();

        //set logic for ban user
        // normal fds
        searchResult.innerHTML += `
        <div class="person nameCard profile" data-id="${user.id}">
        <div class="container-fluid picContainer" style="max-width: 135px">
            <img class="profilePic" src="../avatar/${relation.profile_pic}.png">
        </div>  
        <div class="person-text">
            <div class="person-info">
                <div class="person-name">
                    ${user.nickname}
                </div>
            </div>
            <div class="action" data-userid="${user.id}">
                <div data-id="${user.id}" class="viewButton">VIEW</div>
            </div>
        </div>
        </div>
        `
    }
    await getButtons();
}


const enableInteraction = async function () {
    // like
    const thumbUps = Array.from(document.querySelectorAll('.like'));
    for (let thumbUp of thumbUps) {
        thumbUp.onclick = async function () {
            const id = thumbUp.getAttribute('data-id');
            const res = await fetch(`/like/${id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ success: true })
            });
            if (thumbUp.classList.contains("green")) {
                thumbUp.classList.remove('green')
                thumbUp.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.innerHTML) - 1
            } else {
                if (thumbUp.nextElementSibling.nextElementSibling.classList.contains("red")) {
                    thumbUp.nextElementSibling.nextElementSibling.classList.remove("red");
                    thumbUp.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML) - 1
                }
                thumbUp.classList.add('green')
                // console.log(thumbUp.nextElementSibling.innerHTML)
                thumbUp.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.innerHTML) + 1
            }
            result = await res.json();
            // await loadVoiceMessage();
        }
    }
    // dislike
    const thumbDowns = Array.from(document.querySelectorAll('.dislike'));
    for (let thumbDown of thumbDowns) {
        thumbDown.onclick = async function () {
            const id = thumbDown.getAttribute('data-id');
            const res = await fetch(`/dislike/${id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ success: true })
            });
            if (thumbDown.classList.contains("red")) {
                thumbDown.classList.remove('red')
                thumbDown.nextElementSibling.innerHTML = parseInt(thumbDown.nextElementSibling.innerHTML) - 1
            } else {
                if (thumbDown.previousElementSibling.previousElementSibling.classList.contains("green")) {
                    thumbDown.previousElementSibling.previousElementSibling.classList.remove("green");
                    thumbDown.previousElementSibling.innerHTML = parseInt(thumbDown.previousElementSibling.innerHTML) - 1
                }
                thumbDown.classList.add('red')
                thumbDown.nextElementSibling.innerHTML = parseInt(thumbDown.nextElementSibling.innerHTML) + 1
            }
            result = await res.json();
            // await loadVoiceMessage();
        }
    }
    // comment
    const commentButtons = Array.from(document.querySelectorAll('.comment'));
    for (let commentButton of commentButtons) {
        commentButton.onclick = async function () {
            const id = commentButton.getAttribute('data-id');
            const res = await fetch(`/voiceMessage/${id}`);
            const selectedAudio = await res.json();



            // console.log(selectedAudio[0])
            await loadComment(selectedAudio[0], commentButton)
            // commentButton.onclick = async function(){
            // document.querySelector('#comment-submit').addEventListener('click',async function(){
            //     document.querySelector('.comment-count').data(selectedAudio[0].id).html() += +1
            //     })
            // }
            // })
        }
    }

    $(document).on('click', '.comment', async function () {
        // document.querySelector('#comment-box-container').classlist.remove('d-none')
        $('.comment-box-container').addClass('active')
    })

}

// -----end of like and dislike logic------

$(document).on('click', '.fa-times-circle', function () {
    // document.querySelector('#comment-box-container').classlist.remove('d-none')
    $('.comment-box-container').removeClass('active')
});


// -----Delete(Hide)------

const messageDeleteButton = async () => {
    // console.log("in del btn function")
    const messageDeleteBtns = document.querySelectorAll('.messageTrashBtn')
    for (let messageDeleteBtn of messageDeleteBtns) {
        messageDeleteBtn.addEventListener('click', async () => {
            id = messageDeleteBtn.getAttribute('data-id')
            const res = await fetch(`/voiceMessage/${id}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ success: true })
            });
            result = await res.json();
            alert('deleted!')
            // document.location.href = '/html/viewGlobalMessage.html'
            location.reload();
        })
    }


}

const commentDeleteButton = () => {
    const commentDeleteBtns = document.querySelectorAll('.commentTrashBtn')
    for (let commentDeleteBtn of commentDeleteBtns) {
        commentDeleteBtn.addEventListener('click', async () => {
            id = commentDeleteBtn.getAttribute('data-id')
            const res = await fetch(`/comment/${id}`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ success: true })
            });
            result = await res.json();
            alert('deleted!')
            location.reload();
        })
    }
}

async function loadComment(selectedAudio, commentButton) {
    const res = await fetch(`/comment/${selectedAudio.id}`)
    const comments = await res.json();
    // console.log('selectedAudio:', selectedAudio.id)
    // console.log('comments:', comments)

    const userResult = await fetch(`/user/${selectedAudio.users_id}`);
    const user = await userResult.json();
    // console.log('user:', user)

    const ownerResult = await fetch('/current-user');
    const owner = await ownerResult.json();
    // console.log('owner:', owner)

    // document.querySelector('#comment-box-container').innerHTML = '';
    document.querySelector('#comment-box-container').innerHTML = `
        <div class="comment-box" id="comment-box-1">
        <div id="close-comment-box"><i class="fas fa-times-circle"></i></div>

        <div>
        <img class = "icon" src="../avatar/${user.profile_pic}.png">
            <div>${user.nickname}</div>
            <audio controls>
                <source src="../uploads/${selectedAudio.message}" type="audio/mpeg">
            </audio>
        </div>
        <form action="/comment" method="POST" id="comment-form">
        <div>
            <img class = "self-icon" src="../avatar/${owner.profile_pic}.png">
            <div>${owner.nickname}</div>
            <textarea id="commentInput" name="commentInput" placeholder="leave your comment here..." required></textarea>
        </div>
        <div id="submit-button-container">
            <input id="comment-submit" type="submit" value="Reply">
        </div>
        </form>
        <hr>
        <div id="response-container">
        </div>
        </div>
    `



    // document.querySelectorAll('.comment').addEventListener('click',async function(){
    //     document.querySelector('#comment-box-container').classlist.remove('d-none')
    // })


    document.querySelector('#response-container').innerHTML = '';
    for (comment of comments) {
        document.querySelector('#response-container').innerHTML += `
        <div>
            <img class="response-icon" src="../avatar/${comment.profile_pic}.png">
            <div class="response-name">${comment.nickname}</div>
            <div class="response-text">${comment.comment}</div>
            <div class="deleteComment" data-id="${comment.id}">${owner.id === comment.users_id || owner.id === 1 ? `<i class="fas fa-trash-alt commentTrashBtn" data-id="${comment.id}"></i>` : ""}</div>
        </div>`
    }

    commentDeleteButton()

    document.querySelector('#comment-form').addEventListener('submit', async function (event) {
        try {
            event.preventDefault();
            if (document.querySelector('#commentInput').checkValidity()) {
                const form = this;
                const formObject = {};
                formObject['messageId'] = selectedAudio.id
                formObject['comment'] = form.commentInput.value;
                const res = await fetch('/comment', {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(formObject)
                });
                loadComment(selectedAudio, commentButton);
                commentButton.nextElementSibling.innerHTML = parseInt(commentButton.nextElementSibling.innerHTML) + 1;
                const result = await res.json()
            }
        } catch (e) {
            // console.log(e);
            res.json({ message: "failed comment" })
        }
    })
}

// function swapText() {
//     var x = document.getElementById("switch");
//     if (x.innerHTML === "Search Global") {
//       x.innerHTML = "Search within friends";
//     } else {
//       x.innerHTML = "Search Global";
//     }
//   }

// nend to set condition for global 
// change add to delete if currently is fd`

loadView();
search();




// ----- mobile-topBar add active class ----- //
$(document).on('click', '.mobile-topBar', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})

// ----- mobile-friendButton add active class ----- //
$(document).on('click', '.mobile-friendButton', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})
// ----- mobile-friendButton remove active class ----- //
$(document).on('click', '.friend', function () {
    if ($('.mobile-friendButton').hasClass('active')) {
        $('.mobile-friendButton').removeClass('active')
    }
})
$(document).on('click', '.fa-times', function () {
    if ($('.mobile-friendButton').hasClass('active')) {
        $('.mobile-friendButton').removeClass('active')
    }
})