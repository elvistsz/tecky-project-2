

let allFriends = []
// ------ google sign in form ------ //

const getCurrentUser = async () => {
    const res = await fetch('/current-user');
    return await res.json();
}


//-------------------------Friend List-------------------------

const friendList = document.querySelector('.friends')
const friendContainer = document.querySelector('.friend-container')
const content = document.querySelector('.content')
const jumpIn = document.querySelector('.jump-in')

const loadFriendList = async function () {
    friendList.innerHTML = ""
    const users = await fetch('/allUsers')
    await loadPendingFriend();
    await loadCurrentFriend();
    await loadBlockFriend();

    const friends = document.querySelectorAll('.friend')

    for (let friend of friends) {
        friend.addEventListener('click', async () => {
            let res = await fetch(`/profile/${friend.getAttribute('data-id')}`)
            let user = await res.json();

            res = await fetch(`/relationship/${user.id}`)
            let relation = (await res.json())[0];

            //set condition and switch case

            jumpIn.innerHTML = `
            <div class="returnBtn"><i class="fas fa-undo-alt"></i>BACK</div>
            <div class="row profile jump-in" style="position:relative" data-id="${user.id}">
                <div class="col-sm-12 personProfile">
            
                    <!----------------- Profile Pic ------------>
                    <div class="container-fluid profile-Container">
                        <img class="profilePic" src="../avatar/${user.profile_pic}.png">
                    </div>
                    <div class="profile-text">
                        <div class="profile-info">
                            <div class="profile-name">
                                ${user.nickname}
                            </div>
                            <div class="six-buttons">
                                <div class="set1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="comment-box-container" class="comment-box-container">
                    <div class="comment-box" id="comment-box-1">
                        <div id="close-comment-box" class="closeButton"><i class="fas fa-times-circle closeButton"></i></div>
                        <div>
                            <img class="icon" src="../avatar/cat.jpg">
                            <div>name</div>
                            <audio controls>
                                <source src="../uploads/" type="audio/mpeg">
                            </audio>
                        </div>
                        <form action="/comment" method="POST" id="comment-form">
                            <div>
                                <div class="self-icon"></div>
                                <div>name</div>
                                <textarea id="commentInput" name="commentInput" placeholder="leave your comment here..."
                                    required></textarea>
                            </div>
                            <div id="submit-button-container">
                                <input id="comment-submit" type="submit" value="Reply">
                            </div>
                        </form>
                        <hr>
                        <div id="response-container">
                            <div>
                                <img class="response-icon" src="../user.png">
                                <div class="response-name"> cat </div>
                                <div class="response-text"></div>
                            </div>
                            <div>
                                <img class="response-icon" src="../user.png">
                                <div class="response-name">name</div>
                                <div class="response-text"></div>
                            </div>
                        </div>
                    </div>
                </div>
            

            </div>
            <div class="jump-in">
            </div>`

            const buttons = document.querySelector('.set1')
  
            if (relation.status === 0) {
                if (relation.action_users_id === user.id) {
                    buttons.innerHTML = `
                    <div class="accept" data-id="${user.id}"><i class="fas fa-plus"></i>Accept friend request</div>
                    <div class="reject" data-id="${user.id}"><i class="fas fa-minus"></i>Decline friend request</div>
                    `
                } else {
                    buttons.innerHTML = `
                    <div class="pending" data-id="${user.id}">pending</div>
                    `
                }
            } else if (relation.status === 1) {
                buttons.innerHTML = `   
                <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Block User</div>
                <div class="rmFrd" data-id="${user.id}"><i class="fas fa-minus"></i>Remove friend</div>
                `
            } else if (relation.status === 2) {
                buttons.innerHTML = `    
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            } else if (relation.status === 3) {
                if (relation.action_users_id !== user.id) {
                    buttons.innerHTML = `   
                <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Unblock User</div>
                `
                }
            } else {
                buttons.innerHTML = `
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            }
            // <!---------------END OF Profile Pic ------------>
            res = await fetch(`/voiceMessage/user/${user.id}`)
            let messages = await res.json()

            for (let message of messages) {

            const likeAndDislikeCount = await fetch(`/likeCount/${message.id}`);
            const likeCountResult = await likeAndDislikeCount.json()

            const res = await fetch(`/likeOrDislike/${message.id}`)
            interactionStatus = await res.json();

                jumpIn.innerHTML += `    
                
                    <div class="col-sm-12 posts profile">
                        <div class="row">

                            <img class="post-profilePic" src="../avatar/${user.profile_pic}.png">
                            <div class="post-text">
                                <div class="post-person-name">
                                    ${user.nickname}
                                </div>
                                <div class="post-time">
                                ${(new Date(message.updated_at)).toDateString()} ${(new Date(message.updated_at)).toLocaleTimeString()}
                                </div>
                            </div>
                        </div>
                        <div class="row post-inner-content"">
                        <div class="post-content">
                            <img class="emoji" src="../emoji/${message.emoji}.png">
                                <div class="audioWave">
                                <audio controls>
                                                <source src="../uploads/${message.message}"
                                                    type="audio/mpeg">
                                            </audio>
                                </div>
                                <div class="col-sm-12 interaction LDC-button" id= "interaction-${message.id}">
                                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                                </div>
                            </div>
                        </div>
                    </div>`

            if (interactionStatus[0]){
                if (interactionStatus[0].like_or_dislike){
                    document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like green" data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                } else if(interactionStatus[0].like_or_dislike===false){
                    document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike red" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                } else if (interactionStatus[0].like_or_dislike===null){
                    document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                }
            }
        }

            await enableInteraction();
            await messageDeleteButton();
            await relationshipBtns();
            let returnBtn = document.querySelector('.returnBtn')
            returnBtn.addEventListener('click', () => {
                location.reload();
            })
        })
    }


    $(document).on('click', '.returnBtn', async () => {
        location.reload();
    })
}


const messageDeleteButton = async()=> {
    const messageDeleteBtns = document.querySelectorAll('.messageTrashBtn')
    for (let messageDeleteBtn of messageDeleteBtns){
        messageDeleteBtn.addEventListener('click', async() => {
            id = messageDeleteBtn.getAttribute('data-id')
            const res = await fetch(`/voiceMessage/${id}`,{
                method: "POST",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({success: true})
            }); 
            result = await res.json();
            alert('deleted!')
            location.reload();
        })
    }   
}


const commentDeleteButton = ()=>{
    const commentDeleteBtns = document.querySelectorAll('.commentTrashBtn')
    for (let commentDeleteBtn of commentDeleteBtns){
        commentDeleteBtn.addEventListener('click', async() => {
            id = commentDeleteBtn.getAttribute('data-id')
            const res = await fetch(`/comment/${id}`,{
                method: "DELETE",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({success: true})
            }); 
            result = await res.json();
            alert('deleted!')
            location.reload();
        })
    }
}


const loadPendingFriend = async () => {
    const res = await fetch('/pendingFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend pending" data-id="${friend.friendId}">${friend.friendUsername}</div>`
    }
}

const loadCurrentFriend = async () => {
    const res = await fetch('/currentFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend current" data-id="${friend.friendId}">${friend.friendUsername}</div>`
    }
}

const loadBlockFriend = async () => {
    const res = await fetch('/blockFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend block" data-id="${friend.friendId}">${friend.friendUsername}</div>`
    }
}


loadFriendList();

const loadMyProfile = async () => {
    $('.jump-in').addClass('d-none')
    let currentUser = await getCurrentUser()
    let res = await fetch(`/profile/${currentUser.id}`)
    let user = await res.json();
    jumpIn.innerHTML = `
                <div class="row profile" style="position:relative">
                <div class="col-sm-12 personProfile">
                    <!----------------- Profile Pic ------------>
                    <div class="container-fluid profile-Container">
                        <img class="profilePic" src="../avatar/${user.profile_pic}.png">
                    </div>  
                    <div class="profile-text">
                        <div class="profile-info">
                            <div class="profile-name">
                                ${user.nickname}
                            </div>
                            <div class="six-buttons">
                                <div class="set1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`
    // <!---------------END OF Profile Pic ------------>
    res = await fetch(`/voiceMessage/user/${currentUser.id}`)
    let messages = await res.json()


    for (let message of messages) {

        const likeAndDislikeCount = await fetch(`/likeCount/${message.id}`);
        const likeCountResult = await likeAndDislikeCount.json()

        const res = await fetch(`/likeOrDislike/${message.id}`)
        interactionStatus = await res.json();    

        jumpIn.innerHTML += `
                <div class="col-sm-12 posts profile">
                    <div class="row">
                        <img class="post-profilePic" src="../avatar/${user.profile_pic}.png">
                        <div class="post-text">
                            <div class="post-person-name">
                                ${user.nickname}
                            </div>
                            <div class="deletePost" data-id="${message.users_id}">${user.id === message.users_id || user.id === 1?`<i class="fas fa-trash-alt messageTrashBtn" data-id="${message.id}"></i>`:""}</div>
                            <div class="post-time">
                                ${(new Date(message.updated_at)).toDateString()} ${(new Date(message.updated_at)).toLocaleTimeString()}
                            </div>
                        </div>
                    </div>
                    <div id="comment-box-container" class="comment-box-container"></div>
                    <div class="row post-inner-content">
                    <div class="post-content">
                    <img class="emoji" src="../emoji/${message.emoji}.png">
                            <div class="audioWave">
                            <audio controls>
                                            <source src="../uploads/${message.message}"
                                                type="audio/mpeg">
                                        </audio>
                            </div>
                            <div class="col-sm-12 interaction LDC-button" id= "interaction-${message.id}">
                                <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                                <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                                <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                                <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                                <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                                <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                            </div>
                            
                        </div>
                    </div>
                </div>`;

            if (interactionStatus[0]){
                if (interactionStatus[0].like_or_dislike){
                    document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like green" data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                } else if(interactionStatus[0].like_or_dislike===false){
                    document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike red" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                } else if (interactionStatus[0].like_or_dislike===null){
                    document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                }
            }
    }

    $('.jump-in').removeClass('d-none')
    await enableInteraction()
    await messageDeleteButton()
}

const enableInteraction = async function(){
    // like
    const thumbUps = Array.from(document.querySelectorAll('.like'));
    for (let thumbUp of thumbUps){
        thumbUp.onclick = async function(){
            const id = thumbUp.getAttribute('data-id');
            const res = await fetch(`/like/${id}`,{
                method: "PUT",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({success: true})
            });
            if(thumbUp.classList.contains("green")){
                thumbUp.classList.remove('green')
                thumbUp.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.innerHTML)-1
            } else {
                if (thumbUp.nextElementSibling.nextElementSibling.classList.contains("red")){
                    thumbUp.nextElementSibling.nextElementSibling.classList.remove("red");
                    thumbUp.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML) - 1 
                }
                thumbUp.classList.add('green')
                thumbUp.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.innerHTML) + 1
            }
            result = await res.json();
        }
    }
    // dislike
    const thumbDowns = Array.from(document.querySelectorAll('.dislike'));
    for (let thumbDown of thumbDowns){
        thumbDown.onclick = async function(){
            const id = thumbDown.getAttribute('data-id');
            const res = await fetch(`/dislike/${id}`,{
                method: "PUT",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({success: true})
            });
            if(thumbDown.classList.contains("red")){
                thumbDown.classList.remove('red')
                thumbDown.nextElementSibling.innerHTML = parseInt(thumbDown.nextElementSibling.innerHTML)-1
            } else {
                if (thumbDown.previousElementSibling.previousElementSibling.classList.contains("green")){
                    thumbDown.previousElementSibling.previousElementSibling.classList.remove("green");
                    thumbDown.previousElementSibling.innerHTML = parseInt(thumbDown.previousElementSibling.innerHTML)-1
                }
                thumbDown.classList.add('red')
                thumbDown.nextElementSibling.innerHTML = parseInt(thumbDown.nextElementSibling.innerHTML)+1
            }
            result = await res.json();
        }
    }
    // comment
    const commentButtons = Array.from(document.querySelectorAll('.comment'));
    for (let commentButton of commentButtons){
        commentButton.onclick= async function(){
            const id = commentButton.getAttribute('data-id');
            const res = await fetch(`/voiceMessage/${id}`);
            const selectedAudio = await res.json();
            await loadComment(selectedAudio[0],commentButton)
        } 
    }

    $(document).on('click','.comment', async function(){
        $('.comment-box-container').addClass('active')
    })

}

$(document).on('click','.fa-times-circle', function(){
    $('.comment-box-container').removeClass('active')
});



async function loadComment(selectedAudio,commentButton){
    const res = await fetch(`/comment/${selectedAudio.id}`)
    const comments = await res.json();


    const userResult = await fetch(`/user/${selectedAudio.users_id}`);
    const user = await userResult.json();

    const ownerResult = await fetch('/current-user');
    const owner = await ownerResult.json();

    document.querySelector('#comment-box-container').innerHTML = `
    <div class="comment-box" id="comment-box-1">
    <div id="close-comment-box" class="closeButton"><i class="fas fa-times-circle closeButton"></i></div>

        <div>
        <img class = "icon" src="../avatar/${user.profile_pic}.png">
            <div>${user.nickname}</div>
            <audio controls>
                <source src="../uploads/${selectedAudio.message}" type="audio/mpeg">
            </audio>
        </div>
        <form action="/comment" method="POST" id="comment-form">
        <div>
            <img class = "self-icon" src="../avatar/${owner.profile_pic}.png">
            <div>${owner.nickname}</div>
            <textarea id="commentInput" name="commentInput" placeholder="leave your comment here..." required></textarea>
        </div>
        <div id="submit-button-container">
            <input id="comment-submit" type="submit" value="Reply">
        </div>
        </form>
        <hr>
        <div id="response-container">
        </div>
        </div>
    `



    document.querySelector('#response-container').innerHTML = '';
    for(comment of comments){
        document.querySelector('#response-container').innerHTML +=`
        <div>
            <img class="response-icon" src="../avatar/${comment.profile_pic}.png">
            <div class="response-name">${comment.nickname}</div>
            <div class="response-text">${comment.comment}</div>
            <div class="deleteComment" data-id="${comment.id}">${owner.id === comment.users_id || owner.id === 1?`<i class="fas fa-trash-alt commentTrashBtn" data-id="${comment.id}"></i>`:""}</div>
        </div>`
    }

    commentDeleteButton();

    document.querySelector('#comment-form').addEventListener('submit', async function(event){
        try{
            event.preventDefault();
            if(document.querySelector('#commentInput').checkValidity()){
                const form = this;
                const formObject = {};
                formObject['messageId'] = selectedAudio.id
                formObject['comment'] = form.commentInput.value;
                const res = await fetch('/comment',{
                    method: "POST",
                    headers:{
                        "Content-Type":"application/json"
                    },
                    body: JSON.stringify(formObject)
                });
                loadComment(selectedAudio,commentButton);
                commentButton.nextElementSibling.innerHTML = parseInt(commentButton.nextElementSibling.innerHTML)+1;
                const result = await res.json()
            }
        }catch(e){
            res.json({message: "failed comment"})
        }
    })
}

//Buttons show in profile and relationship function

const add = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Request sent')
    document.location.href = '/html/profile.html'
}

const unfriend = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('unfriended')
    document.location.href = '/html/profile.html'
}

const blockOrUnBlock = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    if (result.message === "User Blocked"){
        alert('User Blocked')
        document.location.href = '/html/profile.html'
    } else {
        alert('User unBlocked')
        document.location.href = '/html/profile.html'
    }
}

const accept = async (id) => {
    const res = await fetch(`/accept/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Accepted')
    document.location.href = '/html/profile.html'
}

const decline = async (id) => {
    const res = await fetch(`/decline/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Declined')
    document.location.href = '/html/profile.html'
}

const relationshipBtns = async () => {
    let addFrdBtns = document.querySelectorAll('.addFrd')
    let acceptBtns = document.querySelectorAll('.accept')
    let rejectBtns = document.querySelectorAll('.reject')
    let blockBtns = document.querySelectorAll('.block')
    let rmFrdBtns = document.querySelectorAll('.rmFrd')

    for (let addFrdBtn of addFrdBtns) {
        addFrdBtn.addEventListener('click', () => {
            let id = addFrdBtn.getAttribute('data-id')
            add(id);
        })
    }
    for (let acceptBtn of acceptBtns) {
        acceptBtn.addEventListener('click', () => {
            let id = acceptBtn.getAttribute('data-id')
            accept(id);
        })
    }
    for (let rejectBtn of rejectBtns) {
        rejectBtn.addEventListener('click', () => {
            let id = rejectBtn.getAttribute('data-id')
            decline(id);
        })
    }
    for (let blockBtn of blockBtns) {
        blockBtn.addEventListener('click', () => {
            let id = blockBtn.getAttribute('data-id')
            blockOrUnBlock(id);
        })
    }
    for (let rmFrdBtn of rmFrdBtns) {
        rmFrdBtn.addEventListener('click', () => {
            let id = rmFrdBtn.getAttribute('data-id')
            unfriend(id);
        })
    }
}

loadMyProfile();

// ------- add class active for .friend ------ //
$(document).on('click', '.friend', function () {
    if ($('.friend').hasClass('active')) {
        $('.friend').removeClass('active')
        $(this).addClass('active');
    } else {
        $(this).addClass('active');
    }
})

// ------ remove class active for .friend when click buttons within navbar ------ //
$(document).on('click', '.button', function () {
    if ($('.friend').hasClass('active')) {
        $('.friend').removeClass('active')
    }
})

// ------ top sticky navbar --- //
window.onscroll = function () {
    if (window.pageYOffset > 45) {
        $('.topBar').addClass('flip')
    } else {
        $('.topBar').removeClass('flip')
    }

}

$(document).on('click', '.switch', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})

// ----- mobile-topBar add active class ----- //
$(document).on('click', '.mobile-topBar', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})

// ----- mobile-friendButton add active class ----- //
$(document).on('click', '.mobile-friendButton', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})
// ----- mobile-friendButton remove active class ----- //
$(document).on('click', '.friend', function () {
    if ($('.mobile-friendButton').hasClass('active')){
        $('.mobile-friendButton').removeClass('active')
    }
})
$(document).on('click', '.fa-times', function () {
    if ($('.mobile-friendButton').hasClass('active')){
        $('.mobile-friendButton').removeClass('active')
    }
})