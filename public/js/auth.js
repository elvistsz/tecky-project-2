async function checkLogin() {
    const res = await fetch('/current-user');
    const user = await res.json();
    if (!user.message) {
        signOutLogo()
        $(document).on('click', '.fa-sign-out-alt', async function () {
            const res = await fetch('/logout');
            alert("logged out");
            document.location.href = '/'
        })
    } else {
        document.location.href = '/'
    }
}

const redirectLink = async function () {
    const res = await fetch('/current-user');
    const user = await res.json();
    if (!user.message) {
        let pages = [['#homePage', 'home'], ['#profilePage', 'profile'], ['#editPage', 'editprofile'], ['#searchPage', 'friendspage'], ['#contactPage', 'searchpage']]
        for (let page of pages) {
            $(document).on('click', page[0], function () {
                document.location.href = `/html/${page[1]}.html`
            })
        }
    } else {
        let pages = ['#homePage', '#profilePage', '#friendPage', '#editPage', '#searchPage', '#contactPage']
        for (let page of pages) {
            $(document).on('click', page, function () {
                if ($('.form').hasClass('active')) {
                    $(this).removeClass('active')
                    $('.form').removeClass('active')
                } else {
                    $(this).addClass('active');
                    $('.form').addClass('active')
                }
            })
        }
    }

    $(document).on('click', '.hole', function () {
        document.location.href = '/'
    })
    $(document).on('click', '.logo', function () {
        document.location.href = '/'
    })


}

function signOutLogo() {
    let signOutLogos = document.querySelectorAll('#loginPanel')
    for (let signOutLogo of signOutLogos) {
        signOutLogo.innerHTML = `<div><i class="fas fa-sign-out-alt button"></i></div>`
    }
    console.log(signOutLogos);
}

checkLogin();
redirectLink();