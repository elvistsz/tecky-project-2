// ------ google sign in form ------ //

const getCurrentUser = async () => {
    const res = await fetch('/current-user');
    return await res.json();
}

// ------- add class active for .friend ------ //
$(document).on('click', '.friend', function () {
    if ($('.friend').hasClass('active')) {
        $('.friend').removeClass('active')
        $(this).addClass('active');
    } else {
        $(this).addClass('active');
    }
})

// ------ remove class active for .friend when click buttons within navbar ------ //
$(document).on('click', '.button', function () {
    if ($('.friend').hasClass('active')) {
        $('.friend').removeClass('active')
    }
})

// ------ top sticky navbar --- //
window.onscroll = function(){
    if (window.pageYOffset > 45){
        $('.topBar').addClass('flip')
    } else {
        $('.topBar').removeClass('flip')
    }
}

// ------ avatar list add active ------//
$(document).on('click', '.avatarEditButton', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        // $('.saveButton').removeClass('active')
    } else {
        $(this).addClass('active');
        $('.saveButton').addClass('active')
    }
})

$(document).on('click', '.avatarPic', function () {
    $('.avatarEditButton').removeClass('active')
})


// ------ edit Nickname add active ------//
$(document).on('click', '.editNickname', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        $('.person-name').removeClass('active')
        checkForSave();
    } else {
        $(this).addClass('active');
        $('.person-name').addClass('active')
        checkForSave();
    }
})
// ------ edit Hometown add active ------//
$(document).on('click', '.editHometown', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        $('.person-location').removeClass('active')
        checkForSave();
    } else {
        $(this).addClass('active');
        $('.person-location').addClass('active')
        checkForSave();
    }
})
// ------ edit Email add active ------//
$(document).on('click', '.editEmail', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        $('.person-email').removeClass('active')
        checkForSave();
    } else {
        $(this).addClass('active');
        $('.person-email').addClass('active')
        checkForSave();
    }
})
// ------ edit Password add active ------//
$(document).on('click', '.editPassword', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        $('.person-password').removeClass('active')
        checkForSave();
    } else {
        $(this).addClass('active');
        $('.person-password').addClass('active')
        checkForSave();
    }
})

// ----- check all active class ----- //
function checkForSave(){
    if($('.active').length > 1){
        $('.saveButton').addClass('active')
    }
}
// ----- remove active class for saveButton ----- //
$(document).on('click', '.saveButton', function () {
    $(this).removeClass('active');
    $('.person-name').removeClass('active');
    $('.person-location').removeClass('active');
    $('.person-email').removeClass('active');
    $('.person-password').removeClass('active');
    $('.avatarEditButton').removeClass('active');
    $('.fa-cog').removeClass('active');
})

// ---------------edit profile-------------------// 

$(document).on('click','.editNickname',function(){
    if($('.person-name').attr("contenteditable")==="false"){
        $('.person-name').attr("contenteditable","true")
        $('.person-name').attr("spellcheck","false")
    } else{
        $('.person-name').attr("contenteditable","false")
    }
})

$(document).on('click','.editHometown',function(){
    if($('.person-location').attr("contenteditable")==="false"){
        $('.person-location').attr("contenteditable","true")
        $('.person-location').attr("spellcheck","false")
    } else{
        $('.person-location').attr("contenteditable","false")
    }
})

$(document).on('click','.editEmail',function(){
    if($('.person-email').attr("contenteditable")==="false"){
        $('.person-email').attr("contenteditable","true")
        $('.person-email').attr("spellcheck","false")
    } else{
        $('.person-email').attr("contenteditable","false")
    }
})

$(document).on('click','.editPassword',function(){
    if($('.person-password').attr("contenteditable")==="false"){
        $('.person-password').attr("contenteditable","true")
        $('.person-password').attr("spellcheck","false")
    } else{
        $('.person-password').attr("contenteditable","false")
    }
})


// ---------Begin of icon----------

$(document).on('click','#cat', function(){
    $('#profilePicture-hidden').val('cat');
    $('#profilePic').attr('src','../avatar/cat.png')
})

$(document).on('click','#dog', function(){
    $('#profilePicture-hidden').val('dog')
    $('#profilePic').attr('src','../avatar/dog.png')
})

$(document).on('click','#frog', function(){
    $('#profilePicture-hidden').val('frog')
    $('#profilePic').attr('src','../avatar/frog.png')
})

$(document).on('click','#giraffee', function(){
    $('#profilePicture-hidden').val('giraffee')
    $('#profilePic').attr('src','../avatar/giraffee.png')
})

$(document).on('click','#hippo', function(){
    $('#profilePicture-hidden').val('hippo')
    $('#profilePic').attr('src','../avatar/hippo.png')
})

$(document).on('click','#lion', function(){
    $('#profilePicture-hidden').val('lion')
    $('#profilePic').attr('src','../avatar/lion.png')
})

$(document).on('click','#monkey', function(){
    $('#profilePicture-hidden').val('monkey')
    $('#profilePic').attr('src','../avatar/monkey.png')
})

$(document).on('click','#pig', function(){
    $('#profilePicture-hidden').val('pig')
    $('#profilePic').attr('src','../avatar/pig.png')
})

$(document).on('click','#sheep', function(){
    $('#profilePicture-hidden').val('sheep')
    $('#profilePic').attr('src','../avatar/sheep.png')
})

$(document).on('click','#tiger', function(){
    $('#profilePicture-hidden').val('tiger')
    $('#profilePic').attr('src','../avatar/tiger.png')
})

// ---------End of icon----------


document.querySelector('#saveButton').addEventListener('click',async function(req,res){
    document.querySelector('#submit-hidden').click();
})


function copyContent(){
    document.querySelector('#nickname-hidden').value = document.querySelector("#nickname-input").innerHTML;
    document.querySelector('#email-hidden').value = document.querySelector("#email-input").innerHTML;
    document.querySelector('#password-hidden').value = document.querySelector("#password-input").innerHTML;
    return true;
}

document.querySelector('#profile-form').addEventListener('submit',async function(event){
    event.preventDefault();

    copyContent()

    const form = this;
    const formObject ={};
    formObject['nickname'] = form.nickname.value.trim();
    formObject['email'] = form.email.value.trim();
    formObject['location'] = form.location.value.trim();
    formObject['password'] = form.password.value.trim();
    formObject['profilePicture'] = form.profilePicture.value.trim();

    const res = await fetch('/profile',{
        method: "POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body: JSON.stringify(formObject)
    });

    location.reload();
    const result = await res.json();
})


async function loadProfile(){
    const currentUserResult = await fetch(`/newProfile`);
    const currentUser = await currentUserResult.json();

    $('#profilePic').attr('src',`../avatar/${currentUser.profile_pic}.png`)
    // const currentUserResult = await fetch(`/current-user`);
    // const currentUser = await currentUserResult.json();

    document.querySelector('#person-text').innerHTML = '';
    document.querySelector('#person-text').innerHTML = `<div class="editContent">
    <div class="editButton">
        NICKNAME
        <i class="fas fa-cog editNickname"></i>
    </div>
    <div class="person-info">
        <div class="person-name" id="nickname-input" contenteditable="false" value="elvis ho">
            ${currentUser.nickname}
        </div>
    </div>
    <div class="person-info">
    </div>
</div>`


    document.querySelector('#person-contact').innerHTML = '';
    document.querySelector('#person-contact').innerHTML = `
        <div class="col-sm-6 editContent">
            <div id="style-contact">
                <div class="editButton" contenteditable="false">
                    EMAIL ADDRESS
                    <i class="fas fa-cog editEmail"></i>
                </div>
                <div class="person-info">
                    <div class="person-email" id="email-input" contenteditable="false">
                        ${currentUser.email}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 editContent">    
            <div id="style-contact">
                <div class="editButton" contenteditable="false">
                    ENTER NEW PASSWORD
                    <i class="fas fa-cog editPassword"></i>
                </div>
                <div class="person-info">
                    <div class="person-password" id="password-input" contenteditable="false">
                        
                    </div>
                </div>
            </div>
        </div>

    `

}


loadProfile();

//-----------------End of edit profile---------------//



// End of buttons show in selected profile and relationship function


// ----- mobile-topBar add active class ----- //
$(document).on('click', '.mobile-topBar', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})

// ----- mobile-friendButton add active class ----- //
$(document).on('click', '.mobile-friendButton', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})
// ----- mobile-friendButton remove active class ----- //
$(document).on('click', '.friend', function () {
    if ($('.mobile-friendButton').hasClass('active')){
        $('.mobile-friendButton').removeClass('active')
    }
})
$(document).on('click', '.fa-times', function () {
    if ($('.mobile-friendButton').hasClass('active')){
        $('.mobile-friendButton').removeClass('active')
    }
})
