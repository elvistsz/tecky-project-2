async function checkLogin() {
    const res = await fetch('/current-user');
    const user = await res.json();
    if (!user.message) {
        signOutLogo()
        $(document).on('click', '.fa-sign-out-alt', async function () {
            const res = await fetch('/logout');
            alert("logged out");
            document.location.href = '/'
        })
    } else {
        $(document).on('click', '.fa-sign-in-alt', function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active')
                $('.form').removeClass('active')
                $('.invisibleBg').removeClass('active')
                $('.invisibleBg').addClass('d-none')
            } else {
                $(this).addClass('active');
                $('.form').addClass('active')
                $('.invisibleBg').removeClass('d-none')
                $('.invisibleBg').addClass('active')
            }
        })
    }
}

const redirectLink = async function () {
    const res = await fetch('/current-user');
    const user = await res.json();
    if (!user.message) {
        let pages = [['#homePage', 'home'], ['#profilePage', 'profile'], ['#editPage', 'editprofile'], ['#searchPage', 'friendspage'], ['#contactPage', 'searchpage']]
        for (let page of pages) {
            $(document).on('click', page[0], function () {
                document.location.href = `/html/${page[1]}.html`
            })
        }
        // console.log("user message exits")
        $(document).on('click', '#homePage', function () {
            document.location.href = '/html/home.html'
        })
        $(document).on('click', '#profilePage', function () {
            document.location.href = '/html/profile.html'
        })
        $(document).on('click', '#editPage', function () {
            document.location.href = '/html/editprofile.html'
        })
        $(document).on('click', '#searchPage', function () {
            document.location.href = '/html/friendspage.html'
        })
        $(document).on('click', '#contactPage', function () {
            document.location.href = '/html/searchpage.html'
        })
    } else {
        let pages = ['#homePage', '#profilePage', '#friendPage', '#editPage', '#searchPage', '#contactPage']
        for (let page of pages) {
            $(document).on('click', page, function () {
                if ($('.form').hasClass('active')) {
                    $(this).removeClass('active')
                    $('.form').removeClass('active')
                } else {
                    $(this).addClass('active');
                    $('.form').addClass('active')
                }
            })
        }
    }
    $(document).on('click', '.hole', function () {
        document.location.href = '/'
    })
    $(document).on('click', '.logo', function () {
        document.location.href = '/'
    })
}


checkLogin();
redirectLink();

function signOutLogo() {
    let signOutLogos = document.querySelectorAll('#loginPanel')
    for (let signOutLogo of signOutLogos) {
        signOutLogo.innerHTML = `<div><i class="fas fa-sign-out-alt button"></i></div>`
    }
}

let createAccount = document.querySelector('.createAccount')
createAccount.addEventListener('click', () => {
    document.querySelector('#login-form').classList.add('d-none')
    document.querySelector('#signUp-form').classList.remove('d-none')
})

let returnToLogin = document.querySelector('.returnToLogin')
returnToLogin.addEventListener('click', () => {
    document.querySelector('#signUp-form').classList.add('d-none')
    document.querySelector('#login-form').classList.remove('d-none')
})

$(document).on('click', '.invisibleBg.active', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        $('.form').removeClass('active')
        $('.invisibleBg').removeClass('active')
        $('.invisibleBg').addClass('d-none')
        $('.fa-sign-in-alt').removeClass('active')
    }
})

window.onscroll = function () {
    if (window.pageYOffset > 45) {
        $('.topBar').addClass('flip')
    } else {
        $('.topBar').removeClass('flip')
    }
}

//----------- MOBILE RESPONSIVE TOP BAR ADD ACTIVE CLASS -----------//

$(document).on('click', '.mobile-topBar', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})