const getCurrentUser = async () => {
    const res = await fetch('/current-user');
    return await res.json();
}

window.onscroll = function () {
    if (window.pageYOffset > 45) {
        $('.topBar').addClass('flip')
    } else {
        $('.topBar').removeClass('flip')
    }
}

//Recorder 

URL = window.URL || window.webkitURL;

var gumStream; 						//stream from getUserMedia()
var rec; 							//Recorder.js object
var input; 							//MediaStreamAudioSourceNode we'll be recording

// shim for AudioContext when it's not avb. 
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext 

var recordButton = document.getElementById("recordButton");
var stopButton = document.getElementById("stopButton");

recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);


let timeout;

async function startRecording() {
    if (recordButton.disabled) {
        return
    }
    let old = document.querySelectorAll('.testing')
    // console.log("olddsdasdejthrgmrdoij", old);
    if (old.length >= 1) {
        old[0].remove()
    }
    // pauseButton.classList.remove('d-none's)
    stopButton.classList.remove('d-none')
    recordButton.classList.add('selected')
    // console.log("recordButton clicked");
	/*
		Simple constraints object, for more advanced audio features see
		https://addpipe.com/blog/audio-constraints-getusermedia/
	*/
    var constraints = { audio: true, video: false }
    /*
      Disable the record button until we get a success or fail from getUserMedia() 
  */
    recordButton.disabled = true;
    stopButton.disabled = false;
    // pauseButton.disabled = false
	/*
    	We're using the standard promise based getUserMedia() 
    	https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
	*/
    navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
        // console.log("getUserMedia() success, stream created, initializing Recorder.js ...");
		/*
			create an audio context after getUserMedia is called
			sampleRate might change after getUserMedia is called, like it does on macOS when recording through AirPods
			the sampleRate defaults to the one set in your OS for your playback device
		*/
        audioContext = new AudioContext();
        //update the format 
        // document.getElementById("formats").innerHTML="Format: 1 channel pcm @ "+audioContext.sampleRate/1000+"kHz"
        /*  assign to gumStream for later use  */
        gumStream = stream;
        /* use the stream */
        input = audioContext.createMediaStreamSource(stream);
		/* 
			Create the Recorder object and configure to record mono sound (1 channel)
			Recording 2 channels  will double the file size
		*/
        rec = new Recorder(input, { numChannels: 1 })
        //start the recording process
        rec.record()

    }).catch(function (err) {
        recordButton.disabled = false;
        stopButton.disabled = true;
    });

    await setupTimer();
    await startTimer();

    timeout = setTimeout(function () {
        if (rec.recording) { stopRecording() }
    }, 60000)

}

function stopRecording() {

    recordButton.classList.remove('selected')
    // pauseButton.classList.remove('selected')
    stopButton.classList.add('d-none')
    // pauseButton.classList.add('d-none')

    //disable the stop button, enable the record too allow for new recordings
    stopButton.disabled = true;
    recordButton.disabled = false;
    // pauseButton.disabled = true;

    //reset button just in case the recording is stopped while paused
    // pauseButton.innerHTML="Pause";

    //tell the recorder to stop the recording
    rec.stop();

    //stop microphone access
    gumStream.getAudioTracks()[0].stop();

    //create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(createDownloadLink);

    clearTimeout(timeout);

    onTimesUp()

    $('#progress-bar').html('')
}


async function createDownloadLink(blob) {

    var url = URL.createObjectURL(blob);
    var img = document.createElement('img')
    var au = document.createElement('audio');
    var br = document.createElement('br');
    var li = document.createElement('div');
    var link = document.createElement('a');

    li.classList.add("testing")

    var filename = new Date().toISOString();

    //add controls to the <audio> element
    au.controls = true;

    au.src = url;

    //add emotions
    let emoji = randomEmotions()

    var emojiIcon = document.createElement('div');
    emojiIcon.classList.add('emojiDefault')
    emojiIcon.innerHTML = `
            <img id="message-emoji" src="../emoji/${emoji}.png">
            <div class="emojiSet">
                <div class="emojiEditButton">
                    <i class="fas fa-cog"></i>
                </div>
                <div class="emojilist">
                    <div class="picFrame" id="angry">
                        <img class="emojiPic" src="../emoji/angry.png">
                    </div>
                    <div class="picFrame" id="calm">
                        <img class="emojiPic" src="../emoji/calm.png">
                    </div>
                    <div class="picFrame" id="disgust">
                        <img class="emojiPic" src="../emoji/disgust.png">
                    </div>
                    <div class="picFrame" id="fearful">
                        <img class="emojiPic" src="../emoji/fearful.png">
                    </div>
                    <div class="picFrame" id="happy">
                        <img class="emojiPic" src="../emoji/happy.png">
                    </div>
                    <div class="picFrame" id="neutral">
                        <img class="emojiPic" src="../emoji/neutral.png">
                    </div>
                    <div class="picFrame" id="sad">
                        <img class="emojiPic" src="../emoji/sad.png">
                    </div>
                    <div class="picFrame" id="surprised">
                        <img class="emojiPic" src="../emoji/surprised.png">
                    </div>
                </div>
            </div>
        `;
    li.appendChild(emojiIcon);



    $(document).on('click', '#angry', function () {
        $('#profilePicture-hidden').val('angry');
        $('#message-emoji').attr('src', '../emoji/angry.png')
        emoji = "angry"
    })

    $(document).on('click', '#calm', function () {
        $('#profilePicture-hidden').val('calm');
        $('#message-emoji').attr('src', '../emoji/calm.png')
        emoji = "calm"
    })

    $(document).on('click', '#disgust', function () {
        $('#profilePicture-hidden').val('disgust');
        $('#message-emoji').attr('src', '../emoji/disgust.png')
        emoji = "disgust"
    })

    $(document).on('click', '#fearful', function () {
        $('#profilePicture-hidden').val('fearful');
        $('#message-emoji').attr('src', '../emoji/fearful.png')
        emoji = "fearful"
    })

    $(document).on('click', '#happy', function () {
        $('#profilePicture-hidden').val('happy');
        $('#message-emoji').attr('src', '../emoji/happy.png')
        emoji = "happy"
    })

    $(document).on('click', '#neutral', function () {
        $('#profilePicture-hidden').val('neutral');
        $('#message-emoji').attr('src', '../emoji/neutral.png')
        emoji = "neutral"
    })

    $(document).on('click', '#sad', function () {
        $('#profilePicture-hidden').val('sad');
        $('#message-emoji').attr('src', '../emoji/sad.png')
        emoji = "sad"
    })

    $(document).on('click', '#surprised', function () {
        $('#profilePicture-hidden').val('surprised');
        $('#message-emoji').attr('src', '../emoji/surprised.png')
        emoji = "surprised"
    })

    var audiodiv = document.createElement('div');
  
    audiodiv.innerHTML = `
    <div>
        <div>
            <audio controls src="${url}"></audio>
        </div>
        <div class="DLPSBtn">
            <a href="${url}" download="${filename}">download</a>
            <a class="postaudio" id="postaudio"href>post</a>
        </div>
    </div>`;

    li.appendChild(audiodiv);


    //upload link
    var upload = document.createElement('a');

    li.appendChild(upload)//add the upload link to li

    let control = document.querySelector('#controls');

    control.appendChild(li);

    var post = document.querySelector('#postaudio')
    //----------------------------------------------------------------------------//
    post.addEventListener("click", async function (event) {
        const formData = new FormData();
        formData.append("audioData", blob, filename);
        formData.append("emoji", emoji)
        const res = await fetch('/uploadAudio', {
            method: "POST",
            body: formData
        })
        const result = await res.json();
    })

    recordingsList.innerHTML = '';

}

// ----- emoji list add active ------ //

$(document).on('click', '.emojiEditButton', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})

$(document).on('click', '.emojiPic', function () {
    $('.emojiEditButton').removeClass('active')
})
// ----- END OF emoji list add active ------ //


// ------------------------Countdown timer--------------------------
const FULL_DASH_ARRAY = 283;
const WARNING_THRESHOLD = 10;
const ALERT_THRESHOLD = 5;

const COLOR_CODES = {
    info: {
        color: "green"
    },
    warning: {
        color: "orange",
        threshold: WARNING_THRESHOLD
    },
    alert: {
        color: "red",
        threshold: ALERT_THRESHOLD
    }
};

const TIME_LIMIT = 60;
let timePassed = 0;
let timeLeft = TIME_LIMIT;
let timerInterval = null;
let remainingPathColor = COLOR_CODES.info.color;

function setupTimer() {

    document.getElementById("progress-bar").innerHTML = `
    <div class="base-timer">
    <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
        <g class="base-timer__circle">
        <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45"></circle>
        <path
            id="base-timer-path-remaining"
            stroke-dasharray="283"
            class="base-timer__path-remaining ${remainingPathColor}"
            d="
            M 50, 50
            m -45, 0
            a 45,45 0 1,0 90,0
            a 45,45 0 1,0 -90,0
            "
        ></path>
        </g>
    </svg>
    <span id="base-timer-label" class="base-timer__label">${formatTime(
        timeLeft
    )}</span>
    </div>
    `;

}


function onTimesUp() {
    timeLeft = 60;
    timePassed = 0;
    clearInterval(timerInterval);
}

function startTimer() {
    timerInterval = setInterval(() => {
        timePassed = timePassed += 1;
        timeLeft = TIME_LIMIT - timePassed;
        document.getElementById("base-timer-label").innerHTML = formatTime(
            timeLeft
        );
        setCircleDasharray();
        setRemainingPathColor(timeLeft);

        if (timeLeft === 0) {
            onTimesUp();
        }
    }, 1000);
}

function formatTime(time) {
    const minutes = Math.floor(time / 60);
    let seconds = time % 60;

    if (seconds < 10) {
        seconds = `0${seconds}`;
    }

    return `${minutes}:${seconds}`;
}

function setRemainingPathColor(timeLeft) {
    const { alert, warning, info } = COLOR_CODES;
    if (timeLeft <= alert.threshold) {
        document
            .getElementById("base-timer-path-remaining")
            .classList.remove(warning.color);
        document
            .getElementById("base-timer-path-remaining")
            .classList.add(alert.color);
    } else if (timeLeft <= warning.threshold) {
        document
            .getElementById("base-timer-path-remaining")
            .classList.remove(info.color);
        document
            .getElementById("base-timer-path-remaining")
            .classList.add(warning.color);
    }
}

function calculateTimeFraction() {
    const rawTimeFraction = timeLeft / TIME_LIMIT;
    return rawTimeFraction - (1 / TIME_LIMIT) * (1 - rawTimeFraction);
}

function setCircleDasharray() {
    const circleDasharray = `${(
        calculateTimeFraction() * FULL_DASH_ARRAY
    ).toFixed(0)} 283`;
    document
        .getElementById("base-timer-path-remaining")
        .setAttribute("stroke-dasharray", circleDasharray);
}
// ------------------------End of Countdown timer--------------------------

//audio player1

let messageWall = document.querySelector('.messageWall')


let allAudio = [];

const loadVoiceMessage = async function () {
    messageWall.innerHTML = '';
    const res = await fetch('/voiceMessage')
    allAudio = await res.json();

    $('.messageWall').addClass('d-none')
    
    for (let i = 0; i < allAudio.length; i++) {
        await displayVoiceMessage(allAudio[i])
    }
    $('.messageWall').removeClass('d-none')
    await enableInteraction();
    await messageDeleteButton();
}

const displayVoiceMessage = async function (selectedAudio) {
    const ownerResult = await fetch('/current-user');
    const owner = await ownerResult.json();

    const userResult = await fetch(`/user/${selectedAudio.users_id}`);
    const user = await userResult.json();

    const likeAndDislikeCount = await fetch(`/likeCount/${selectedAudio.id}`);
    const likeCountResult = await likeAndDislikeCount.json()

    const res = await fetch(`/likeOrDislike/${selectedAudio.id}`)
    interactionStatus = await res.json();


    messageWall.innerHTML += `
       
<div class="message col-sm-12 posts profile" data-id="${selectedAudio.id}">
    <div class="row">
        <img class="post-profilePic" src="../avatar/${user.profile_pic}.png">
        <div class="post-text">

            <div class="post-person-name">
                ${user.nickname}
            </div>
            <div class="deletePost" data-id="${selectedAudio.users_id}">${owner.id === selectedAudio.users_id || owner.id === 1 ? `<i class="fas fa-trash-alt messageTrashBtn" data-id="${selectedAudio.id}"></i>` : ""}</div>
            <div class="post-time">
                ${(new Date(selectedAudio.updated_at)).toDateString()} ${(new Date(selectedAudio.updated_at)).toLocaleTimeString()}
            </div>
        </div>
    </div>
    <div class="row post-padding">
        <div class="post-content">
        <img class="emoji" src="../emoji/${selectedAudio.emoji}.png">
            <div class="audioWave voice">
                <audio controls>
                <source src="../uploads/${selectedAudio.message}"
                    type="audio/mpeg">
                </audio>
            </div>
            <div class="col-sm-12 interaction LDC-button" id= "interaction-${selectedAudio.id}">
                <div class="L-button like " data-id=${selectedAudio.id}><i class="far fa-thumbs-up"></i>like</div>
                <div class="like-count" data-id="${selectedAudio.id}">${likeCountResult.like}</div>
                <div class="D-button  dislike" data-id=${selectedAudio.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                <div class="dislike-count"data-id="${selectedAudio.id}">${likeCountResult.dislike}</div>
                <div class="C-button comment "data-id="${selectedAudio.id}"><i class="far fa-comments " ></i>Comment</div>
                <div class="comment-count"data-id="${selectedAudio.id}">${likeCountResult.comment}</div>
            </div>
        </div>
    </div>
</div>`;


    if (interactionStatus[0] === undefined) {
        return
    }


    if (interactionStatus[0].like_or_dislike) {
        document.querySelector(`#interaction-${selectedAudio.id}`).innerHTML = `
        <div class="L-button like green" data-id=${selectedAudio.id}><i class="far fa-thumbs-up"></i>like</div>
        <div class="like-count" data-id="${selectedAudio.id}">${likeCountResult.like}</div>
        <div class="D-button  dislike" data-id=${selectedAudio.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
        <div class="dislike-count"data-id="${selectedAudio.id}">${likeCountResult.dislike}</div>
        <div class="C-button comment "data-id="${selectedAudio.id}"><i class="far fa-comments " ></i>Comment</div>
        <div class="comment-count"data-id="${selectedAudio.id}">${likeCountResult.comment}</div>
        `
    } else if (interactionStatus[0].like_or_dislike === false) {
        document.querySelector(`#interaction-${selectedAudio.id}`).innerHTML = `
        <div class="L-button like " data-id=${selectedAudio.id}><i class="far fa-thumbs-up"></i>like</div>
        <div class="like-count" data-id="${selectedAudio.id}">${likeCountResult.like}</div>
        <div class="D-button  dislike red" data-id=${selectedAudio.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
        <div class="dislike-count"data-id="${selectedAudio.id}">${likeCountResult.dislike}</div>
        <div class="C-button comment "data-id="${selectedAudio.id}"><i class="far fa-comments " ></i>Comment</div>
        <div class="comment-count"data-id="${selectedAudio.id}">${likeCountResult.comment}</div>
        `
    } else if (interactionStatus[0].like_or_dislike === null) {
        document.querySelector(`#interaction-${selectedAudio.id}`).innerHTML = `
        <div class="L-button like " data-id=${selectedAudio.id}><i class="far fa-thumbs-up"></i>like</div>
        <div class="like-count" data-id="${selectedAudio.id}">${likeCountResult.like}</div>
        <div class="D-button  dislike" data-id=${selectedAudio.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
        <div class="dislike-count"data-id="${selectedAudio.id}">${likeCountResult.dislike}</div>
        <div class="C-button comment "data-id="${selectedAudio.id}"><i class="far fa-comments " ></i>Comment</div>
        <div class="comment-count"data-id="${selectedAudio.id}">${likeCountResult.comment}</div>
        `
    }
}

// -----like and dislike logic------
const enableInteraction = async function () {
    // like
    const thumbUps = Array.from(document.querySelectorAll('.like'));
    for (let thumbUp of thumbUps) {
        thumbUp.onclick = async function () {
            const id = thumbUp.getAttribute('data-id');
            const res = await fetch(`/like/${id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ success: true })
            });
            if (thumbUp.classList.contains("green")) {
                thumbUp.classList.remove('green')
                thumbUp.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.innerHTML) - 1
            } else {
                if (thumbUp.nextElementSibling.nextElementSibling.classList.contains("red")) {
                    thumbUp.nextElementSibling.nextElementSibling.classList.remove("red");
                    thumbUp.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML) - 1
                }
                thumbUp.classList.add('green')
                thumbUp.nextElementSibling.innerHTML = parseInt(thumbUp.nextElementSibling.innerHTML) + 1
            }
            result = await res.json();
        }
    }
    // dislike
    const thumbDowns = Array.from(document.querySelectorAll('.dislike'));
    for (let thumbDown of thumbDowns) {
        thumbDown.onclick = async function () {
            const id = thumbDown.getAttribute('data-id');
            const res = await fetch(`/dislike/${id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ success: true })
            });
            if (thumbDown.classList.contains("red")) {
                thumbDown.classList.remove('red')
                thumbDown.nextElementSibling.innerHTML = parseInt(thumbDown.nextElementSibling.innerHTML) - 1
            } else {
                if (thumbDown.previousElementSibling.previousElementSibling.classList.contains("green")) {
                    thumbDown.previousElementSibling.previousElementSibling.classList.remove("green");
                    thumbDown.previousElementSibling.innerHTML = parseInt(thumbDown.previousElementSibling.innerHTML) - 1
                }
                thumbDown.classList.add('red')
                thumbDown.nextElementSibling.innerHTML = parseInt(thumbDown.nextElementSibling.innerHTML) + 1
            }
            result = await res.json();
        }
    }
    // comment
    const commentButtons = Array.from(document.querySelectorAll('.comment'));
    for (let commentButton of commentButtons) {
        commentButton.onclick = async function () {
            const id = commentButton.getAttribute('data-id');
            const res = await fetch(`/voiceMessage/${id}`);
            const selectedAudio = await res.json();
            await loadComment(selectedAudio[0], commentButton)
        }
    }

    $(document).on('click', '.comment', async function () {
        $('.comment-box-container').addClass('active')
    })

}

// -----end of like and dislike logic------

$(document).on('click', '.fa-times-circle', function () {
    $('.comment-box-container').removeClass('active')
});


// -----Delete(Hide)------

const messageDeleteButton = async () => {
    const messageDeleteBtns = document.querySelectorAll('.messageTrashBtn')
    for (let messageDeleteBtn of messageDeleteBtns) {
        messageDeleteBtn.addEventListener('click', async () => {
            id = messageDeleteBtn.getAttribute('data-id')
            const res = await fetch(`/voiceMessage/${id}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ success: true })
            });
            result = await res.json();
            alert('deleted!')
            location.reload();
        })
    }


}

const commentDeleteButton = () => {
    const commentDeleteBtns = document.querySelectorAll('.commentTrashBtn')
    for (let commentDeleteBtn of commentDeleteBtns) {
        commentDeleteBtn.addEventListener('click', async () => {
            id = commentDeleteBtn.getAttribute('data-id')
            const res = await fetch(`/comment/${id}`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ success: true })
            });
            result = await res.json();
            alert('deleted!')
            location.reload();
        })
    }
}

async function loadComment(selectedAudio, commentButton) {
    const res = await fetch(`/comment/${selectedAudio.id}`)
    const comments = await res.json();
    const userResult = await fetch(`/user/${selectedAudio.users_id}`);
    const user = await userResult.json();
    const ownerResult = await fetch('/current-user');
    const owner = await ownerResult.json();
    document.querySelector('#comment-box-container').innerHTML = `
        
        <div class="comment-box" id="comment-box-1">
        <div id="close-comment-box"><i class="fas fa-times-circle"></i></div>
        <div>
        <img class = "icon" src="../avatar/${user.profile_pic}.png">
            <div>${user.nickname}</div>
            <audio controls>
                <source src="../uploads/${selectedAudio.message}" type="audio/mpeg">
            </audio>
        </div>
        <form action="/comment" method="POST" id="comment-form">
        <div>
            <img class = "self-icon" src="../avatar/${owner.profile_pic}.png">
            <div>${owner.nickname}</div>
            <textarea id="commentInput" name="commentInput" placeholder="leave your comment here..." required></textarea>
        </div>
        <div id="submit-button-container">
            <input id="comment-submit" type="submit" value="Reply">
        </div>
        </form>
        <hr>
        <div id="response-container">
        </div>
        </div>
    `



    document.querySelector('#response-container').innerHTML = '';
    for (comment of comments) {
        document.querySelector('#response-container').innerHTML += `
        <div>
            <img class="response-icon" src="../avatar/${comment.profile_pic}.png">
            <div class="response-name">${comment.nickname}</div>
            <div class="response-text">${comment.comment}</div>
            <div class="deleteComment" data-id="${comment.id}">${owner.id === comment.users_id || owner.id === 1 ? `<i class="fas fa-trash-alt commentTrashBtn" data-id="${comment.id}"></i>` : ""}</div>
        </div>`
    }

    commentDeleteButton()

    document.querySelector('#comment-form').addEventListener('submit', async function (event) {
        try {
            event.preventDefault();
            if (document.querySelector('#commentInput').checkValidity()) {
                const form = this;
                const formObject = {};
                formObject['messageId'] = selectedAudio.id
                formObject['comment'] = form.commentInput.value;
                const res = await fetch('/comment', {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(formObject)
                });
                loadComment(selectedAudio, commentButton);
                commentButton.nextElementSibling.innerHTML = parseInt(commentButton.nextElementSibling.innerHTML) + 1;
                const result = await res.json()
            }
        } catch (e) {
            res.json({ message: "failed comment" })
        }
    })
}

// -----end of Delete(Hide)------
loadVoiceMessage();



//-------------------------Friend List-------------------------

const friendList = document.querySelector('.friends')
const friendContainer = document.querySelector('.friend-container')
const content = document.querySelector('.content')
const jumpIn = document.querySelector('.jump-in')

const loadFriendList = async function () {
    friendList.innerHTML = ""
    const users = await fetch('/allUsers')
    await loadPendingFriend();
    await loadCurrentFriend();
    await loadBlockFriend();

    const friends = document.querySelectorAll('.friend')

    for (let friend of friends) {
        friend.addEventListener('click', async () => {
            let res = await fetch(`/profile/${friend.getAttribute('data-id')}`)
            let user = await res.json();

            res = await fetch(`/relationship/${user.id}`)
            let relation = (await res.json())[0];

            // console.log("relation", relation)
            //	0 Pending
            //  1 Accepted
            //  2 Declined
            //  3 Blocked

            //set condition and switch case

            jumpIn.innerHTML = `
                <div class="returnBtn"><i class="fas fa-undo-alt"></i>BACK</div>   
                <div class="row profile" style="position:relative" data-id="${user.id}">
                <div class="col-sm-12 personProfile">
                    <!----------------- Profile Pic ------------>
                    <div class="container-fluid profile-Container">
                        <img class="profilePic" src="../avatar/${user.profile_pic}.png">
                    </div>  
                    <div class="profile-text">
                        <div class="profile-info">
                            <div class="profile-name">
                                ${user.nickname} 
                            </div>
                            <div class="six-buttons">
                                <div class="set1">            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="comment-box-container" class="comment-box-container">
                <div class="comment-box" id="comment-box-1">
                    <div id="close-comment-box" class="closeButton"><i class="fas fa-times-circle closeButton"></i></div>
                    <div>
                        <img class = "icon" src="../avatar/cat.jpg">
                        <div>name</div>
                        <audio controls>
                            <source src="../uploads/" type="audio/mpeg">
                        </audio>
                    </div>
                    <form action="/comment" method="POST" id="comment-form">
                    <div>
                        <div class="self-icon"></div>
                        <div>name</div>
                        <textarea id = "commentInput" name="commentInput" placeholder="leave your comment here..." required></textarea>
                    </div>
                    <div id="submit-button-container">
                        <input id="comment-submit" type="submit" value="Reply">
                    </div>
                    </form>
                    <hr>
                    <div id="response-container">
                        <div>
                            <img class="response-icon" src="../user.png">
                            <div class="response-name"> cat </div>
                            <div class="response-text"></div>
                        </div>
                        <div>
                            <img class="response-icon" src="../user.png">
                            <div class="response-name">name</div>
                            <div class="response-text"></div>
                        </div>
                    </div>
                </div>
            </div>`

            const buttons = document.querySelector('.set1')
            if (relation.status === 0) {
                if (relation.action_users_id === user.id) {
                    buttons.innerHTML = `
                    <div class="accept" data-id="${user.id}"><i class="fas fa-plus"></i>Accept friend request</div>
                    <div class="reject" data-id="${user.id}"><i class="fas fa-minus"></i>Decline friend request</div>
                    `
                } else {
                    buttons.innerHTML = `
                    <div class="pending" data-id="${user.id}">pending</div>
                    `
                }
            } else if (relation.status === 1) {
                buttons.innerHTML = `   
                <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Block User</div>
                <div class="rmFrd" data-id="${user.id}"><i class="fas fa-minus"></i>Remove friend</div>
                `
            } else if (relation.status === 2) {
                buttons.innerHTML = `    
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            } else if (relation.status === 3) {
                if (relation.action_users_id !== user.id) {
                    buttons.innerHTML = `   
                <div class="block" data-id="${user.id}"><i class="fas fa-ban"></i>Unblock User</div>
                `
                }
            } else {
                buttons.innerHTML = `
                <div class="addFrd" data-id="${user.id}"><i class="fas fa-plus"></i>Add friend</div>
                `
            }
            // <!---------------END OF Profile Pic ------------>
            res = await fetch(`/voiceMessage/user/${user.id}`)
            let messages = await res.json()



            // console.log("message", messages)
            for (let message of messages) {

                const likeAndDislikeCount = await fetch(`/likeCount/${message.id}`);
                const likeCountResult = await likeAndDislikeCount.json()

                const res = await fetch(`/likeOrDislike/${message.id}`)
                interactionStatus = await res.json();

                jumpIn.innerHTML += `    
                <div class="message col-sm-12 posts profile">
                <div class="row">
                    <img class="post-profilePic" src="../avatar/${user.profile_pic}.png">
                    <div class="post-text">
                        <div class="post-person-name">
                            ${user.nickname}
                        </div>
                        <div class="post-time">
                            ${(new Date(message.updated_at)).toDateString()} ${(new Date(message.updated_at)).toLocaleTimeString()}
                        </div>
                    </div>
                </div>
                <div class="row post-padding">
                <div class="post-content">
                    <img class="emoji" src="../emoji/${message.emoji}.png">
                        <div class="audioWave voice">
                        <audio controls>
                                        <source src="../uploads/${message.message}"
                                            type="audio/mpeg">
                                    </audio>
                        </div>
                        <div class="col-sm-12 interaction LDC-button" id= "interaction-${message.id}">
                            <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                            <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                            <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                            <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                            <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                            <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                        </div>
                    </div>
                </div>
            </div>`
                if (interactionStatus[0]) {
                    if (interactionStatus[0].like_or_dislike) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like green" data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                    } else if (interactionStatus[0].like_or_dislike === false) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike red" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                    } else if (interactionStatus[0].like_or_dislike === null) {
                        document.querySelector(`#interaction-${message.id}`).innerHTML = `
                    <div class="L-button like " data-id=${message.id}><i class="far fa-thumbs-up"></i>like</div>
                    <div class="like-count" data-id="${message.id}">${likeCountResult.like}</div>
                    <div class="D-button  dislike" data-id=${message.id}><i class="far fa-thumbs-down" ></i>Dislike</div>
                    <div class="dislike-count"data-id="${message.id}">${likeCountResult.dislike}</div>
                    <div class="C-button comment "data-id="${message.id}"><i class="far fa-comments " ></i>Comment</div>
                    <div class="comment-count"data-id="${message.id}">${likeCountResult.comment}</div>
                    `
                    }
                }
            }




            await enableInteraction();
            await messageDeleteButton();
            await relationshipBtns();
            let returnBtn = document.querySelector('.returnBtn')
            returnBtn.addEventListener('click', () => {
                location.reload();
            })
        })
    }

}

const loadPendingFriend = async () => {
    const res = await fetch('/pendingFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend pending" data-id="${friend.friendId}">${friend.friendUsername}</div>`
    }
}

const loadCurrentFriend = async () => {
    const res = await fetch('/currentFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend current" data-id="${friend.friendId}">${friend.friendUsername}</div>`
    }
}

const loadBlockFriend = async () => {
    const res = await fetch('/blockFriend')
    allFriends = await res.json();
    for (let friend of allFriends) {
        friendList.innerHTML += `<div class="friend block" data-id="${friend.friendId}">${friend.friendUsername}</div>`
    }
}

const add = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Request sent')
    document.location.href = '/html/home.html'
}

const unfriend = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('unfriended')
    document.location.href = '/html/home.html'
}

const blockOrUnBlock = async (id) => {
    const res = await fetch(`/friend/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    if (result.message === "User Blocked") {
        alert('User Blocked')
        document.location.href = '/html/home.html'
    } else {
        alert('User unBlocked')
        document.location.href = '/html/home.html'
    }
}

const accept = async (id) => {
    const res = await fetch(`/accept/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Accepted')
    document.location.href = '/html/home.html'
}

const decline = async (id) => {
    const res = await fetch(`/decline/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ success: true })
    })
    const result = await res.json();
    alert('Declined')
    document.location.href = '/html/home.html'
}

const relationshipBtns = async () => {
    let addFrdBtns = document.querySelectorAll('.addFrd')
    let acceptBtns = document.querySelectorAll('.accept')
    let rejectBtns = document.querySelectorAll('.reject')
    let blockBtns = document.querySelectorAll('.block')
    let rmFrdBtns = document.querySelectorAll('.rmFrd')

    for (let addFrdBtn of addFrdBtns) {
        addFrdBtn.addEventListener('click', () => {
            let id = addFrdBtn.getAttribute('data-id')
            add(id);
        })
    }
    for (let acceptBtn of acceptBtns) {
        acceptBtn.addEventListener('click', () => {
            let id = acceptBtn.getAttribute('data-id')
            accept(id);
        })
    }
    for (let rejectBtn of rejectBtns) {
        rejectBtn.addEventListener('click', () => {
            let id = rejectBtn.getAttribute('data-id')
            decline(id);
        })
    }
    for (let blockBtn of blockBtns) {
        blockBtn.addEventListener('click', () => {
            let id = blockBtn.getAttribute('data-id')
            blockOrUnBlock(id);
        })
    }
    for (let rmFrdBtn of rmFrdBtns) {

        rmFrdBtn.addEventListener('click', () => {
            let id = rmFrdBtn.getAttribute('data-id')
            unfriend(id);
        })
    }
}

// End of buttons show in selected profile and relationship function


// Get user icon

const getUserIcon = async () => {
    let userIcon = document.querySelector('#userIcon')
    let user = await getCurrentUser();
    userIcon.innerHTML = `<img class="post-profilePic" src="../avatar/${user.profile_pic}.png">`
}

// End of get user Icon
loadFriendList();
getUserIcon();


//random emotions
const randomEmotions = () => {
    let emoji = ['angry', 'calm', 'disgust', 'fearful', 'happy', 'neutral', 'sad', 'surprised']
    let num = Math.floor(Math.random() * 8)
    return emoji[num]
}

// ----- mobile-topBar add active class ----- //
$(document).on('click', '.mobile-topBar', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})

// ----- mobile-friendButton add active class ----- //
$(document).on('click', '.mobile-friendButton', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active');
    }
})
// ----- mobile-friendButton remove active class ----- //
$(document).on('click', '.friend', function () {
    if ($('.mobile-friendButton').hasClass('active')){
        $('.mobile-friendButton').removeClass('active')
    }
})
$(document).on('click', '.fa-times', function () {
    if ($('.mobile-friendButton').hasClass('active')){
        $('.mobile-friendButton').removeClass('active')
    }
})
