import Knex from 'knex';


export class MessageService{
    constructor(private knex:Knex){}

    async getMessage(){
        return await this.knex.select('*').from('voice_message').where('hidden', '=', false).orderBy('updated_at','desc')
    }

    async getMessageById(id:string){
        return await this.knex('voice_message').select("*").where('id','=',id).andWhere('hidden','=',false);
    }

    async getOwnerId(id:number){
        return (await this.knex('voice_message').select('users_id').where('id', '=', id))[0].users_id
    }

    async deleteMessage(id:number){
        return await this.knex.update({ 'hidden': true }).from('voice_message').where('id', '=', id)
    }

    async loadMessageByUser(id:number){
        return (await this.knex('voice_message').select('*').where('users_id', '=', id).andWhere('hidden','=',false).orderBy('updated_at','desc'))
    }

    async getLikeOrDislike(messageId:string, userId:number){
        return await this.knex.select('*').from('like_or_dislike').where('message_id', '=', messageId).andWhere('users_id', '=', userId);
    }

    async InsertMessage(userId:number, filename:string, emoji:string){
        return await this.knex('voice_message').insert({ users_id: userId, message: filename, hidden: false, emoji: emoji })
    }

    async getLikeStatus(userId:number, messageId:number){
        return await this.knex.select('*').where('users_id', '=', userId).andWhere('message_id', '=', messageId).from('like_or_dislike')
    }

    async cancelExistingLike(userId:number, messageId:number){
        return await this.knex('like_or_dislike').update({ like_or_dislike: null }).where('users_id', '=', userId).andWhere('message_id', '=', messageId)
    }

    async addLikeFromNull(userId:number, messageId:number){
        return await this.knex('like_or_dislike').update({ like_or_dislike: true }).where('users_id', '=', userId).andWhere('message_id', '=', messageId)
    }

    async insertLike(userId:number, messageId:number){
        return await this.knex('like_or_dislike').insert([{ message_id: messageId, users_id: userId, like_or_dislike: true }])
    }

    async addDislikeFromNull(userId:number, messageId:number){
        return await this.knex('like_or_dislike').update({ like_or_dislike: false }).where('users_id', '=', userId).andWhere('message_id', '=', messageId)
    }

    async insertDislike(userId:number, messageId:number){
        return await this.knex('like_or_dislike').insert([{ message_id: messageId, users_id: userId, like_or_dislike: false }])
    }

    async likeResult(id:string){
        return await this.knex.count('message_id').where('message_id', '=', id).andWhere('like_or_dislike', '=', true).from('like_or_dislike')
    }

    async dislikeResult(id:string){
        return await this.knex.count('message_id').where('message_id', '=', id).andWhere('like_or_dislike', '=', false).from('like_or_dislike')
    }

    async commentResult(id:string){
        return await this.knex.count('id').where('message_id', '=', id).from('comment').andWhere('hidden','=',false)
    }

    async selectComment(id:string){
        return await this.knex('users').select('*').join('comment','users_id','users.id').where('message_id','=',id).andWhere('hidden','=',false).orderBy('comment.updated_at','desc')
    }

    async insertComment(messageId:string, userId:string, comment:string){
        return await this.knex('comment').insert([{message_id: messageId, users_id: userId, comment: comment, hidden: false}])
    }

    async deleteComment(id:string){
        return await this.knex('comment').update({hidden: true}).where('id','=',id)
    }

    async getUserByUserId(id:string){
        return await this.knex.select('id', 'username', 'nickname', 'profile_pic', 'email', 'created_at', 'updated_at').from('users').where('id', '=', id);
    }
    
    async updateNickname(nickname:string,userId:number){
        return await this.knex('users').update({nickname: nickname}).where('id','=', userId)
    }

    async updateEmail(email:string, userId:number){
        return await this.knex('users').update({email: email}).where('id','=',userId)
    }

    async updatePassword(hashedPassword:string, userId:number){
        return await this.knex('users').update({password: hashedPassword}).where('id','=',userId)
    }

    async uploadProfilePic(profilePicture:string, userId:number){
        return await this.knex('users').update({profile_pic: profilePicture}).where('id','=',userId)
    }
}