import Knex from 'knex';


export class FriendService{
    constructor(private knex:Knex){}

    async selectAllUsername(){
        return await this.knex.select('username').from('users')
    }

    async currentFriend(userId:number){
        return await this.knex.select('t.id', 't.users_one_id', 't.users_two_id', 't.status', 't.action_users_id', 't1.nickname as users_one_nickname', 't2.nickname as users_two_nickname').from('relationship as t').join('users as t1', 't.users_one_id', 't1.id').join('users as t2', 't.users_two_id', 't2.id').where('t.status', '=', 1).andWhere('t.users_one_id', '=', userId).orWhere('t.status', '=', 1).andWhere('t.users_two_id', '=', userId)
    }

    async pendingFriend(userId:number){
        return await this.knex.select('t.id', 't.users_one_id', 't.users_two_id', 't.status', 't.action_users_id', 't1.nickname as users_one_nickname', 't2.nickname as users_two_nickname').from('relationship as t').join('users as t1', 't.users_one_id', 't1.id').join('users as t2', 't.users_two_id', 't2.id').where('t.status', '=', 0).andWhere('t.users_one_id', '=', userId).orWhere('t.status', '=', 0).andWhere('t.users_two_id', '=', userId)
    }

    async blockedFriend(userId:number){
        return await this.knex.select('t.id', 't.users_one_id', 't.users_two_id', 't.status', 't.action_users_id', 't1.nickname as users_one_nickname', 't2.nickname as users_two_nickname').from('relationship as t').join('users as t1', 't.users_one_id', 't1.id').join('users as t2', 't.users_two_id', 't2.id').where('t.status', '=', 3).andWhere('t.users_one_id', '=', userId).orWhere('t.status', '=', 3).andWhere('t.users_two_id', '=', userId)
    }

    async allFriendExceptDeclined(userId:number){
        return await this.knex.select('t.id', 't.users_one_id', 't.users_two_id', 't.status', 't.action_users_id', 't1.profile_pic as users_one_proPic', 't2.profile_pic as users_two_proPic', 't1.nickname as users_one_nickname', 't2.nickname as users_two_nickname').from('relationship as t').join('users as t1', 't.users_one_id', 't1.id').join('users as t2', 't.users_two_id', 't2.id').whereNot('t.status', '=', 2).andWhere('t.users_one_id', '=',userId).orWhere('t.users_two_id', '=', userId).andWhereNot('t.status', '=', 2)
    }

    async globalUser(userId:number){
        return await this.knex('users').distinct('id').select('*').whereNot('id','=',userId)
    }

    async searchGlobal(userId:number, keyword:String){
        return await this.knex('users').distinct('id').select('*').whereNot('id','=',userId).andWhere('nickname','ilike',`%${keyword}%`)
    }

    async checkDeclined(userId:number, friendId:string){
        return await this.knex('relationship').select('*').where('users_one_id', '=', userId).andWhere('users_two_id', '=', friendId).where('status','=',2)
   }

   async readdFriend(userId:number, friendId:string){
        return await this.knex('relationship').update({status: 0,action_users_id: userId}).where('users_one_id', '=', userId).andWhere('users_two_id', '=', friendId)
   }

   async addFriend(userId:number, friendId:string){
       return await this.knex('relationship').insert({users_one_id: userId, users_two_id: friendId, status: 0, action_users_id: userId})
   }

   async checkRelationshipWithGreaterSelfId(userId:number, friendId:string){
       return await this.knex('relationship').select('*').where('users_one_id', '=', friendId).andWhere('users_two_id', '=', userId)
   }

   async readdFriendWithGreaterSelfId(userId:number,friendId:string){
       return await this.knex('relationship').update({status: 0,action_users_id: userId}).where('users_one_id', '=', friendId).andWhere('users_two_id', '=', userId)
   }

   async addFriendWithGreaterSelfId(userId:number,friendId:string){
       return await this.knex('relationship').insert({users_one_id: friendId, users_two_id: userId, status: 0, action_users_id: userId})
   }

   async searchFriendWithSmallerId(userId:number, keyword:string){
       return await this.knex('users').select('*').join('relationship','users.id','relationship.users_one_id').where('relationship.users_one_id','<',userId).andWhere('relationship.users_two_id','=',userId).andWhere('users.nickname','ilike',`%${keyword}%`)
   }

   async searchFriendWithGreaterId(userId:number, keyword:string){
       return await this.knex('users').select('*').join('relationship','users.id','relationship.users_two_id').where('relationship.users_two_id','>',userId).andWhere('relationship.users_one_id','=',userId).andWhere('users.nickname','ilike',`%${keyword}%`)
   }

   async checkRelationshipWithSmallerSelfId(userId:number, friendId:string){
        return await this.knex('relationship').select('*').where('users_one_id', '=', userId).andWhere('users_two_id', '=', friendId)
   }
   

   async deleteFriendWithSmallerSelfId(userId:number, friendId:string){
        return await this.knex('relationship').where('users_one_id', '=', userId).andWhere('users_two_id', '=', friendId).del()
   }

   async deleteFriendWithGreaterSelfId(userId:number, friendId:string){
       return await this.knex('relationship').where('users_one_id', '=', friendId).andWhere('users_two_id', '=', userId).del()
   }

   async reblockFriendWithSmallerSelfId(userId:number, friendId:string){
       return await this.knex('relationship').update({status: 3,action_users_id: userId}).where('users_one_id', '=', userId).andWhere('users_two_id', '=', friendId)
   }

   async reunblockFriendWithSamllerSelfId(userId:number, friendId:string){
       return await this.knex('relationship').where('users_one_id', '=', userId).andWhere('users_two_id', '=', friendId).del()
   }


   async reblockFriendWithGreaterSelfId(userId:number, friendId:string){
       return await this.knex('relationship').update({status: 3,action_users_id: userId}).where('users_one_id', '=', friendId).andWhere('users_two_id', '=', userId)
    }

    async reunblockFriendWithGreaterSelfId(userId:number, friendId:string){
        return await this.knex('relationship').where('users_one_id', '=', friendId).andWhere('users_two_id', '=', userId).del()
    }

    async declineFriendWithSmallerSelfId(userId:number, friendId:string){
        return await this.knex('relationship').update({status: 2,action_users_id: userId}).where('users_one_id', '=', userId).andWhere('users_two_id', '=', friendId)
    }

    async declineFriendWithGreaterSelfId(userId:number, friendId:string){
        return await this.knex('relationship').update({status: 2,action_users_id: userId}).where('users_one_id', '=', friendId).andWhere('users_two_id', '=', userId)
    }

    async acceptFriendWithSmallerSelfId(userId:number, friendId:string){
        return await this.knex('relationship').update({status: 1,action_users_id: userId}).where('users_one_id', '=', userId).andWhere('users_two_id', '=', friendId)
    }

    async acceptFriendWithGreaterSelfId(userId:number, friendId:string){
        return await this.knex('relationship').update({status: 1,action_users_id: userId}).where('users_one_id', '=', friendId).andWhere('users_two_id', '=', userId)
    }

    async getFriendProfile(friendId:string){
        return (await this.knex('users').select('id', 'nickname', 'profile_pic').where('id', '=', friendId))[0]
    }

    async getNewProfile(userId:number){
        return this.knex.select('nickname','email','profile_pic').from('users').where("id",'=',userId)
    }

    async getRelationshipbyId(userId:number,friendId:string){
        return await this.knex.select('*').from('relationship').where('users_one_id','=', friendId).andWhere('users_two_id','=',userId).orWhere('users_one_id','=', userId).andWhere('users_two_id','=',friendId)
    }
}