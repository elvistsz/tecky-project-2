import Knex from 'knex';
import {randomProPic} from '../main';
import {GoogleUser} from '../models';

export class UserService{
    constructor(private knex:Knex){}

    async login(username:string){
        return await this.knex.select('*').from('users').where('username', '=', `${username}`)
    }

    async checkGmail(googleUser:GoogleUser){
        console.log(`this is checkgamil`)
        return await this.knex.select('*').from('users').where('email', '=', `${googleUser.email}`)
    }

    async createAcByGoogle(googleUser:GoogleUser, randomPassword:string){
        console.log(`this is create ac by google`)
        return await this.knex("users").insert({ username: googleUser.name, password: `${randomPassword}`, profile_pic: randomProPic(), nickname: "anonymous", email: googleUser.email })
    }

    async checkUserExist(username:string){
        console.log(`this is checkUserExist`, (await this.knex("users").select('username').where('username', '=', username)).length !== 0)
        return (await this.knex("users").select('username').where('username', '=', username)).length !== 0
    }

    async checkEmailExist(email:string){
        console.log(`this is checkEmailExist`, (await this.knex("users").select('email').where('email', '=', email)).length !== 0)
        return (await this.knex("users").select('email').where('email', '=', email)).length !== 0
    }

    async signUp(username:string,hashedPassword:string, email:string){
        console.log(`this is signUp`)
        return await this.knex("users").insert({ username: username, password: hashedPassword, profile_pic: randomProPic(), nickname: "anonymous" ,email: email })
    }
}