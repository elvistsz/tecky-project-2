import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    // await knex("like_or_dislike").del();
    // await knex("comment").del();
    // await knex("relationship").del();
    // await knex("voice_message").del();
    // await knex("users").del();
    await knex.raw('TRUNCATE TABLE like_or_dislike RESTART IDENTITY CASCADE')
    await knex.raw('TRUNCATE TABLE comment RESTART IDENTITY CASCADE')
    await knex.raw('TRUNCATE TABLE relationship RESTART IDENTITY CASCADE')
    await knex.raw('TRUNCATE TABLE voice_message RESTART IDENTITY CASCADE')
    await knex.raw('TRUNCATE TABLE users RESTART IDENTITY CASCADE')
    

    await knex("users").insert([
        {username: "admin", password: "$2a$10$RZnaQ1TEPlMWTFJmoJpGwutaTe4PRtQwXd4lv6xAHZIRusbGe2gvi", nickname: "admin", profile_pic: "tiger", email: "admin@treehole.com"},
        {username: "elvis", password: "$2a$10$1iMTYrwUIMo03/U2WBpCbOiM2Inc3QymUQkfiexh7pJCgnNQYMhXS", nickname: "cat", profile_pic: "cat", email: "elvis@treehole.com"},
        {username: "zak", password: "$2a$10$U4Y.xnAM3yJRk9KeLRYcSOSGYFpjDz.r5eFHRlWn7rkFRiF/B99Va", nickname: "Zzzz", profile_pic: "sheep", email: "zak@treehole.com"},
        {username: "bryan", password: "$2a$10$C5tqZd9q4posapctfy2JXu/XlJgzlQ9PgcMZz3easBd0.5c53TyDW", nickname: "BRYAN", profile_pic: "frog", email: "bryan@treehole.com"}
    ]);

    await knex("voice_message").insert([
        {users_id: 1, message: "testing1.mp3", hidden: false, emoji: "happy"},
        {users_id: 1, message: "testing2.mp3", hidden: false, emoji: "sad"},
        {users_id: 1, message: "testing3.mp3", hidden: true, emoji: "calm"},
        {users_id: 2, message: "testing4.mp3", hidden: false, emoji: "fearful"},
        {users_id: 3, message: "testing5.mp3", hidden: false, emoji: "surprised"},
    ]);

    await knex("relationship").insert([
        {users_one_id: 1, users_two_id:2, status:0, action_users_id:1},
        {users_one_id: 2, users_two_id:3, status:1, action_users_id:3},
        {users_one_id: 3, users_two_id:4, status:2, action_users_id:4},
        {users_one_id: 2, users_two_id:4, status:3, action_users_id:4},
        {users_one_id: 1, users_two_id:3, status:1, action_users_id:1},
        {users_one_id: 1, users_two_id:4, status:1, action_users_id:4}
    ]);

    //	0 Pending
	//  1 Accepted
	//  2 Declined
	//  3 Blocked

    await knex("comment").insert([
        {message_id: 1, users_id:2, comment: "good talk!", hidden:false},
        {message_id: 1, users_id:3, comment: "I disagree", hidden:false},
        {message_id: 2, users_id:2, comment: "I would like ur attention!", hidden:false},
        {message_id: 5, users_id:4, comment: "hahaha!", hidden:false},
        {message_id: 3, users_id:1, comment: "well said!", hidden:false}
    ]);

    await knex("like_or_dislike").insert([
        {users_id: 2, message_id: 1, like_or_dislike:true},
        {users_id: 3, message_id: 1, like_or_dislike:true},
        {users_id: 4, message_id: 1, like_or_dislike:true},
        {users_id: 1, message_id: 2, like_or_dislike:false},
        {users_id: 3, message_id: 2, like_or_dislike:false},
        {users_id: 4, message_id: 2, like_or_dislike:false}
    ]);
};
