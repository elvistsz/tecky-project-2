
from fastai.vision import *
import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
from flask_cors import CORS
from flask import Flask


app = Flask(__name__)
CORS(app)

@app.route("/emotions", methods=['POST'])
def predict():
    y, sr = librosa.load('temp.wav')
    yt, _ = librosa.effects.trim(y)
    y = yt
    mel_spect = librosa.feature.melspectrogram(y=y, sr=sr, n_fft=1024, hop_length=100)
    mel_spect = librosa.power_to_db(mel_spect, ref=np.max)
    librosa.display.specshow(mel_spect, y_axis='mel', fmax=20000, x_axis='time');
    plt.savefig('temp.jpeg')
    learn = load_learner('./')
    predictions = learn.predict(open_image('temp.jpeg'))
    print(predictions)
    result = []
    result.append(predictions[0])
    # return json.dumps(str(result[0]))
    return str(result[0])







    # mel_spect = librosa.power_to_db(mel_spect, ref=np.max)

    # plt.savefig('temp.jpeg')
    # result = learn.predict(open_image('temp.jpeg'))
    # print(result)
    # print(result[0])
    # return json.dumps(result)
    # testing
    # return json.dumps(result[0])




    # content = request.get_json()
    # predict_dataset = tf.convert_to_tensor(content)
    # predictions = model(predict_dataset, training=False)
    # results = []
    # for i, logits in enumerate(predictions):
    #     class_idx = tf.argmax(logits).numpy()
    #     p = tf.nn.softmax(logits)[class_idx]
    #     name = class_names[class_idx]
    #     results.append({"name": name,"probability": float(p)})
    # return json.dumps(results)


# y, sr = librosa.load('uploads/audioData-1597461419192.wav')
# yt,_=librosa.effects.trim(y)
#
# y = yt
#
# mel_spect = librosa.feature.melspectrogram(y=y, sr=sr, n_fft=1024, hop_length=100)
# mel_spect = librosa.power_to_db(mel_spect, ref=np.max)
# librosa.display.specshow(mel_spect,y_axis='mel', fmax=20000, x_axis='time');
#
#
# learn = load_learner('./')
# plt.savefig('temp.jpeg')
#
# result =learn.predict(open_image('temp.jpeg'))
# print(result)
# print(result[0])