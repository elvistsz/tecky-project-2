import { UserService } from '../Service/userService'
import { Request, Response } from 'express';
import { checkPassword, hashPassword } from '../hash';
import fetch from 'node-fetch';

export class UserController {
    constructor(public userService: UserService) { }

    post = async (req: Request, res: Response) => {
        try {
            const result = await this.userService.login(req.body.username)
            const found = result[0];
            if (found && await checkPassword(req.body.password, found.password)) {
                if (req.session) {
                    req.session.user = found;
                }
                res.redirect('/html/home.html');
            } else {
                res.status(401).send('<p>Invalid Username / Password</p>');
            }
        } catch (e) {
            console.log(e);
            res.status(500).json({ message: "Login Failed" })
        }
    }


    get = async (req: Request, res: Response) => {
        try {
            console.log(`this is get Google`)
            const accessToken = req.session?.grant.response.access_token;
            const googleResponse = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            const googleUser = await googleResponse.json();
            const result = await this.userService.checkGmail(googleUser)
            const found = result[0];
            if (found) {
                if (req.session) {
                    req.session.user = found;
                }
                res.redirect('/html/home.html');
            } else {
                console.log(`this is googleUser.email`, googleUser.email)
                let randomPassword = await Math.random().toString(36).substring(2);
                await this.userService.createAcByGoogle(googleUser, randomPassword)
                // res.redirect("/login/google")
                const result = await this.userService.checkGmail(googleUser)
                const found = result[0];
                if (found) {
                    if (req.session) {
                        req.session.user = found;
                    }

                    res.redirect('/html/home.html');
                    // res.redirect('/html/index.html');
                    // res.json({message: "login as google"})
                }
            }
        }catch (e) {
            console.log(e);
        }
    }

    signUppost = async (req: Request, res: Response) => {
        try {
            if (req.body && req.session) {
                if (req.body.password !== req.body.confirmPassword) {
                    res.status(401).json({ message: "password must be the same as confirmPassword" })
                    return
                }
                const hashedPassword = await hashPassword(req.body.password)
                if (await this.userService.checkUserExist(req.body.username)) {
                    res.status(401).json({ message: "Username already exist" });
                    return
                }
                if (await this.userService.checkEmailExist(req.body.email)) {
                    res.status(401).json({ message: "Email already exist" });
                    return
                }
                await this.userService.signUp(req.body.username, hashedPassword, req.body.email)
                const result = await this.userService.login(req.body.username)
                const found = result[0];
                req.session.user = found;
                res.redirect('/');
            } else {
                res.status(400);
            }
        } catch (e) {
            console.log(e);
            res.status(500).json({ message: "Login Failed" })
        }
    }

    logOut = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                delete req.session.user;
            }
            res.json("logout: success")
        } catch (e) {
            console.log(e);
        }
    }
}