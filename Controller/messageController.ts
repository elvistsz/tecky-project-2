import {MessageService} from '../Service/messageService';
import {Request, Response} from 'express';
import {hashPassword} from '../hash'




export class MessageController {
    constructor (public messageService:MessageService){}

    get = async (req:Request, res:Response)=>{
        try {
            const voiceMessages = await this.messageService.getMessage();
            res.json(voiceMessages);
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getById = async (req:Request, res:Response)=>{
        try{
            const {id} = req.params;
            const voiceMessage = await this.messageService.getMessageById(id);
            res.json(voiceMessage);
        }catch(e){
            console.log(e);
            res.status(500).json({message: 'Internal Server Error'})
        }
    }

    delete = async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                const id = parseInt(req.params.id)
                const owner = await this.messageService.getOwnerId(id);
                if (req.session.user.id === owner || req.session.user.id === 1) {
                    // console.log("confirm creator/admin")
                    await this.messageService.deleteMessage(id);
                    res.json("success : true");
                } else {
                    res.status(400).json("Bad request")
                }
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    loadUserMessage= async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                const id = parseInt(req.params.id)
                const messages = await this.messageService.loadMessageByUser(id);
                res.json(messages)
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getInteraction = async (req:Request, res:Response)=>{
        const { id } = req.params
        try {
            if (req.session) {
                const likeOrDislike = await this.messageService.getLikeOrDislike(id, req.session.user.id);
                res.json(likeOrDislike)
    
            } else {
                res.status(400).json({ message: "please login in" })
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    createNewMessage = async (req:Request, res:Response)=>{
        try {
            // console.log(req.session?.user)
            if (req.session && req.session.user) {
                await this.messageService.InsertMessage(req.session.user.id, req.file.filename, req.body.emoji)
                res.json("success : true")
            }
        } catch (e) {
            console.log(e)
            res.status(500).json("internal server error")
        }
    }

    like = async (req:Request, res:Response)=>{
        try {
            const id = parseInt(req.params.id)
            if (req.session) {
                const result = await this.messageService.getLikeStatus(req.session.user.id, id)
                // console.log(result)
                if (result[0]) {
                    if (result[0].like_or_dislike === true) {
                        await this.messageService.cancelExistingLike(req.session.user.id, id)
                        // await knex('like_or_dislike').where('users_id','=',req.session.user.id).andWhere('message_id','=',id).del()
                    } else if (result[0].like_or_dislike === false || result[0].like_or_dislike === null) {
                        await this.messageService.addLikeFromNull(req.session.user.id,id )
                    }
                } else {
                    await this.messageService.insertLike(req.session.user.id, id)
                }
            }
            res.json("success : true")
        } catch (e) {
            console.log(e)
            res.status(500).json("internal server error")
        }
    }

    dislike = async (req:Request, res:Response)=>{
        try {
            const id = parseInt(req.params.id)
            if (req.session) {
                const result = await this.messageService.getLikeStatus(req.session.user.id, id)
                // console.log(result)
                if (result[0]) {
                    if (result[0].like_or_dislike === false) {
                        await this.messageService.cancelExistingLike(req.session.user.id, id)
                        // await knex('like_or_dislike').where('users_id','=',req.session.user.id).andWhere('message_id','=',id).del()
                    } else if (result[0].like_or_dislike === true || result[0].like_or_dislike === null) {
                        await this.messageService.addDislikeFromNull(req.session.user.id, id)
                    }
                } else {
                    await this.messageService.insertDislike(req.session.user.id,id)
                }
            }
            res.json("success : true")
        } catch (e) {
            console.log(e)
            res.status(500).json("internal server error")
        }
    }

    likeCount = async (req:Request, res:Response)=>{
        try {
            let messageStat = {}
            const { id } = req.params
            const likeResult = await this.messageService.likeResult(id)
            const dislikeResult = await this.messageService.dislikeResult(id)
            const commentResult = await this.messageService.commentResult(id)
            messageStat = { like: likeResult[0].count, dislike: dislikeResult[0].count, comment: commentResult[0].count }
            res.json(messageStat)
        } catch (e) {
            console.log(e);
            res.status(500).json("internal server error")
        }
    }

    getCommentbyMessageId = async (req:Request, res:Response)=>{
        try{
            const {id} = req.params;
            const comments = await this.messageService.selectComment(id);
            console.log(comments)
            res.json(comments)
        } catch(e){
            console.log(e);
            res.status(500).json({ message: e })
        }
    }

    createNewComment = async (req:Request, res:Response)=>{
        try{
            if(req.session){
                await this.messageService.insertComment(req.body.messageId, req.session.user.id, req.body.comment);
                console.log('comment is inserted')
                res.json({success: true})
            }
            }catch(e){
            console.log(e);
            res.status(500).json({message:"Internal server error"})
        }
    }

    deleteComment = async (req:Request, res:Response)=>{
        try{
            const {id} = req.params;
            if(req.session){
                await this.messageService.deleteComment(id);
                console.log('comment is deleted')
                res.json({success: true})
            }
        }catch(e){
            console.log(e);
            res.status(500).json({message:"Interval server error"})
        }
    }

    getUserbyUserId = async (req:Request, res:Response)=>{
        try {
            const { id } = req.params
            if (req.session) {
                const user = await this.messageService.getUserByUserId(id);
                res.json(user[0])
            } else {
                res.status(400).json({ message: "please login in" })
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    editPorfile = async (req:Request, res:Response)=>{
        try{
            console.log(req.body)
            if(req.session){
                if(req.body.nickname){
                    await this.messageService.updateNickname(req.body.nickname, req.session.user.id)
                }
                if(req.body.email){
                    await this.messageService.updateEmail(req.body.email, req.session.user.id)
                }
                // if(req.body.location){
                //     await knex('users').update({location: req.body.location}).where('id','=',req.session.user.id)
                // }
                if(req.body.password){
                    let hashedPassword = await hashPassword(`${req.body.password}`)
                    console.log(hashedPassword)
                    await this.messageService.updatePassword(hashedPassword,req.session.user.id)
                }
                if(req.body.profilePicture){
                    console.log('profile picture exist')
                    await this.messageService.uploadProfilePic(req.body.profilePicture, req.session.user.id)
                }
                const result = await this.messageService.getUserByUserId(req.session.user.id)
                const found = result[0];
                    if (req.session) {
                        req.session.user = found;
                    }
                res.json({success: true})
                }
        } catch(e){
            console.log(e);
            res.status(500).json({message: "Internal server error"})
        }
    }

    uploadAudioTemp= async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user) {
                res.json("success : true")
            }
        } catch (e) {
            console.log(e)
            res.status(500).json("internal server error")
        }
    }
}
