import {FriendService} from '../Service/friendService';
import {Request, Response} from 'express';
// import Knex from 'knex';



export class FriendController {
    constructor (public friendService:FriendService){}

    getAllUser = async (req:Request, res:Response)=>{
        try {
            const usersDetails = await this.friendService.selectAllUsername();
            res.json(usersDetails);
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getCurrentFriend = async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                const friends = []
                const friendList = await this.friendService.currentFriend(req.session.user.id)
                for (let friend of friendList) {
    
                    if (friend.users_one_id === req.session.user.id) {
                        friends.push({ "friendId": friend.users_two_id, "friendUsername": friend.users_two_nickname })
                    } else {
                        friends.push({ "friendId": friend.users_one_id, "friendUsername": friend.users_one_nickname })
                    }
                }
                res.json(friends);
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getPendingFriend = async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                const friends = []
                const friendList = await this.friendService.pendingFriend(req.session.user.id)
                for (let friend of friendList) {
                    if (friend.users_one_id === req.session.user.id) {
                        friends.push({ "friendId": friend.users_two_id, "friendUsername": friend.users_two_nickname })
                    } else {
                        friends.push({ "friendId": friend.users_one_id, "friendUsername": friend.users_one_nickname })
                    }
                }
                res.json(friends);
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getBlockedFriend = async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                const friends = []
                const friendList = await this.friendService.blockedFriend(req.session.user.id);    
                for (let friend of friendList) {
                    if (friend.users_one_id === req.session.user.id && friend.users_one_id === friend.action_users_id) {
                        friends.push({ "friendId": friend.users_two_id, "friendUsername": friend.users_two_nickname })
                    } else if (friend.users_two_id === req.session.user.id && friend.users_two_id === friend.action_users_id) {
                        friends.push({ "friendId": friend.users_one_id, "friendUsername": friend.users_one_nickname })
                    }
                }
                res.json(friends);
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getFriendExceptDeclined = async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                const friends: any[] = []
                const friendList = await this.friendService.allFriendExceptDeclined(req.session.user.id);
                for (let friend of friendList) {
                    if (friend.users_one_id === req.session.user.id) {
                        let obj = { "id": friend.users_two_id, "nickname": friend.users_two_nickname, "status": friend.status, "action_users": friend.action_users_id, "profile_pic": friend.users_two_proPic }
                        friends.push(obj)
                    } else if (friend.users_two_id === req.session.user.id) {
                        let obj = { "id": friend.users_one_id, "nickname": friend.users_one_nickname, "status": friend.status, "action_users": friend.action_users_id, "profile_pic": friend.users_one_proPic }
                        friends.push(obj)
                    }
                }
                res.json(friends);
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getGlobal = async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                const UsersList = await this.friendService.globalUser(req.session.user.id)
                res.json(UsersList);
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    searchGlobal = async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                let {keyword} = req.params
                console.log('keyword', keyword)
                const UsersList = await this.friendService.searchGlobal(req.session.user.id ,keyword)
                res.json(UsersList);
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    addFriend = async (req:Request, res:Response)=>{
        try {
            if (req.session){
                const {id} = req.params
                if (id >= req.session.user.id){
                    const state = await this.friendService.checkDeclined(req.session.user.id, id)
                    if (state[0]){
                        await this.friendService.readdFriend(req.session.user.id, id);
                    } else {
                        await this.friendService.addFriend(req.session.user.id, id)
                    }
                    res.status(200).json({ message: "Sent request!" });
                } else if ( id <= req.session.user.id){
                    
                    const state = await this.friendService.checkRelationshipWithGreaterSelfId(req.session.user.id, id);
                    if (state[0]){
                        await this.friendService.readdFriendWithGreaterSelfId(req.session.user.id, id)
                    } else {
                        await this.friendService.addFriendWithGreaterSelfId(req.session.user.id, id)
                    }
                    res.status(200).json({ message: "Sent request!" });
                } else {
                    res.status(400).json({ message: "Bad request" });
                }
            } else {
                res.status(400).json({ message: "Bad request" });
            }
        } catch (e){
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    searchFriend = async (req:Request, res:Response)=>{
        try {
            if (req.session && req.session.user.id) {
                const friends: any[] = []
                const {keyword} = req.params
                const friendList1 = await this.friendService.searchFriendWithSmallerId(req.session.user.id, keyword)
                const friendList2 = await this.friendService.searchFriendWithGreaterId(req.session.user.id, keyword)
                console.log(friendList1)
                for (let friend of friendList1){
                    friends.push({ "id": friend.users_one_id, "nickname": friend.nickname }) 
                }    
                for (let friend of friendList2){
                    friends.push({ "id": friend.users_two_id, "nickname": friend.nickname })
                }       
                res.json(friends);
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    deleteFriend = async(req:Request,res:Response)=>{
        try {
            if (req.session){
                const {id} = req.params
                if (id >= req.session.user.id){
                    const state = await this.friendService.checkRelationshipWithSmallerSelfId(req.session.user.id, id)
                    if (state[0].status === 3 && state[0].action_users_id ==  id){
                        res.status(400).json({ message: "Bad request1" });
                        return
                    } else {
                        await this.friendService.deleteFriendWithSmallerSelfId(req.session.user.id, id)
                        res.status(200).json({ message: "User deleted" });
                    }
                } else if ( id <= req.session.user.id){
                    const state = await this.friendService.checkRelationshipWithGreaterSelfId(req.session.user.id, id)
                    if (state[0].status === 3 && state[0].action_users_id ==  id){
                        res.status(400).json({ message: "Bad request1" });
                        return
                    } else {
                        await this.friendService.deleteFriendWithGreaterSelfId(req.session.user.id, id)
                        res.status(200).json({ message: "User deleted" });
                    }
                } else {
                    res.status(400).json({ message: "Bad request3" });
                }
            } else {
                res.status(400).json({ message: "Bad request4" });
            }
        } catch (e){
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    blockAndUnblockFriend = async (req:Request, res:Response)=>{
        try {
            if (req.session){
                const {id} = req.params
                if (id >= req.session.user.id){
                    const state = await this.friendService.checkRelationshipWithSmallerSelfId(req.session.user.id,id);
                    if (state[0].status === 1){
                        await this.friendService.reblockFriendWithSmallerSelfId(req.session.user.id, id)
                        res.status(200).json({ message: "User Blocked" });
                    } else if (state[0].status === 3 && state[0].action_users_id === req.session.user.id){
                        await this.friendService.reunblockFriendWithSamllerSelfId(req.session.user.id,id)
                        res.status(200).json({ message: "User UnBlocked!" });
                    } else {
                        res.status(400).json({ message: "Bad request1" });
                    }
                } else if ( id <= req.session.user.id){
                    const state = await this.friendService.checkRelationshipWithGreaterSelfId(req.session.user.id,id)
                    if (state[0].status === 1){
                        await this.friendService.reblockFriendWithGreaterSelfId(req.session.user.id,id)
                        res.status(200).json({ message: "User Blocked" });
                    } else if (state[0].status === 3 && state[0].action_users_id === req.session.user.id){
                        await this.friendService.reunblockFriendWithGreaterSelfId(req.session.user.id,id);
                        res.status(200).json({ message: "User UnBlocked!" });
                    } else {
                        res.status(400).json({ message: "Bad request2" });
                    }
                } else {
                    res.status(400).json({ message: "Bad request3" });
                }
            } else {
                res.status(400).json({ message: "Bad request4" });
            }
        } catch (e){
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    declineFriend = async (req:Request, res:Response)=>{
        try {
            if (req.session){
                const {id} = req.params
                if (id >= req.session.user.id){
                    const state = await this.friendService.checkRelationshipWithSmallerSelfId(req.session.user.id,id)
                    if (state[0].status === 0 && state[0].action_users_id === parseInt(id)){
                        await this.friendService.declineFriendWithSmallerSelfId(req.session.user.id,id)
                        res.status(200).json({ message: "Request Declined" });
                    } else {
                        res.status(400).json({ message: "Bad request1" });
                    }
                } else if ( id <= req.session.user.id){
                    const state = await this.friendService.checkRelationshipWithGreaterSelfId(req.session.user.id, id)
                    if (state[0].status === 0 && state[0].action_users_id === parseInt(id)){
                        await this.friendService.declineFriendWithGreaterSelfId(req.session.user.id,id)
                        res.status(200).json({ message: "Request Declined" });
                    } else {
                        res.status(400).json({ message: "Bad request2" });
                    }
                } else {
                    res.status(400).json({ message: "Bad request3" });
                }
            } else {
                res.status(400).json({ message: "Bad request4" });
            }
        } catch (e){
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    acceptFriend = async(req:Request, res:Response)=>{
        try {
            if (req.session){
                const {id} = req.params
                if (id >= req.session.user.id){
                    const state = await this.friendService.checkRelationshipWithSmallerSelfId(req.session.user.id, id)
                    if (state[0].status === 0 && state[0].action_users_id == parseInt(id)){
                        await this.friendService.acceptFriendWithSmallerSelfId(req.session.user.id,id)
                        res.status(200).json({ message: "Request Accept" });
                    } else {
                        res.status(400).json({ message: "Bad request1" });
                    }
                } else if ( id <= req.session.user.id){
                    const state = await this.friendService.checkRelationshipWithGreaterSelfId(req.session.user.id,id)
                    if (state[0].status === 0 && state[0].action_users_id === parseInt(id)){
                        await this.friendService.acceptFriendWithGreaterSelfId(req.session.user.id,id)
                        res.status(200).json({ message: "Request Accept" });
                    } else {
                        res.status(400).json({ message: "Bad request2" });
                    }
                } else {
                    res.status(400).json({ message: "Bad request3" });
                }
            } else {
                res.status(400).json({ message: "Bad request4" });
            }
        } catch (e){
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getProfile = async (req:Request, res:Response)=>{
        try {
            const { id } = req.params
            if (req.session) {
                const friend = await this.friendService.getFriendProfile(id);
                res.json(friend)
            } else {
                res.status(400).json({ message: "please login in" })
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }

    getNewProfile = async (req:Request, res:Response)=>{
        try{
            if(req.session){
                const result = await this.friendService.getNewProfile(req.session.user.id)
                // const result = await knex.select('*').from('users').where("id",'=',req.session.user.id)
                res.json(result[0])
            }
        }catch(e){
            console.log(e);
            res.json({message: e});
        }
    }

    getAllRelationship = async (req:Request, res:Response)=>{
        try {
            const {id} = req.params
            if (req.session){
                const relationship = await this.friendService.getRelationshipbyId(req.session.user.id,id)
                res.json(relationship)
            }
        }catch (e){
            console.log(e)
            res.status(500).json({ message: "Internal server error" });
        }
    }
}