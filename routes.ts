import express from 'express';
import {userController, messageController, friendController} from './main';
import { isLoggedInAPI } from './guards';
import {upload, uploadTemp} from './main'

// import { FriendController } from './Controller/friendController';
// import { MessageController } from './Controller/messageController';
// import { UserController } from './Controller/userController';


export const userRoutes = express.Router();

userRoutes.post('/login',userController.post);
userRoutes.get('/login/google',userController.get);
userRoutes.post('/signUp',userController.signUppost);
userRoutes.get('/logout',userController.logOut);

export const messageRoutes = express.Router();

messageRoutes.get('/voiceMessage',isLoggedInAPI,messageController.get);
messageRoutes.get('/voiceMessage/:id',isLoggedInAPI,messageController.getById);
messageRoutes.post('/voiceMessage/:id', isLoggedInAPI,messageController.delete); 
messageRoutes.get('/voiceMessage/user/:id', isLoggedInAPI,messageController.loadUserMessage);
messageRoutes.post('/uploadAudio', isLoggedInAPI, upload.single('audioData'), messageController.createNewMessage);


messageRoutes.get('/likeOrDislike/:id', isLoggedInAPI, messageController.getInteraction);
messageRoutes.put('/like/:id', isLoggedInAPI, messageController.like);
messageRoutes.put('/dislike/:id', isLoggedInAPI, messageController.dislike);
messageRoutes.get('/likeCount/:id', isLoggedInAPI, messageController.likeCount);

messageRoutes.get('/comment/:id',isLoggedInAPI, messageController.getCommentbyMessageId);
messageRoutes.post('/comment',isLoggedInAPI,messageController.createNewComment);
messageRoutes.delete('/comment/:id',isLoggedInAPI,messageController.deleteComment);

messageRoutes.get('/user/:id', isLoggedInAPI, messageController.getUserbyUserId);
messageRoutes.post('/profile', isLoggedInAPI, messageController.editPorfile);

messageRoutes.post('/audioTemp', isLoggedInAPI, uploadTemp.single('audio'), messageController.uploadAudioTemp);

export const friendRoutes = express.Router();

friendRoutes.get('/allUsers', isLoggedInAPI,friendController.getAllUser);
friendRoutes.get('/currentFriend', isLoggedInAPI,friendController.getCurrentFriend);
friendRoutes.get('/pendingFriend', isLoggedInAPI, friendController.getPendingFriend);
friendRoutes.get('/blockFriend', isLoggedInAPI, friendController.getBlockedFriend);

friendRoutes.get('/friend', isLoggedInAPI,friendController.getFriendExceptDeclined); 
friendRoutes.get('/global', isLoggedInAPI, friendController.getGlobal);
friendRoutes.get('/users/global/:keyword', isLoggedInAPI, friendController.searchGlobal);
friendRoutes.post('/friend/:id',isLoggedInAPI,friendController.addFriend);
friendRoutes.get('/users/friends/:keyword', isLoggedInAPI, friendController.searchFriend);
friendRoutes.delete('/friend/:id', isLoggedInAPI, friendController.deleteFriend);
friendRoutes.put('/friend/:id',isLoggedInAPI, friendController.blockAndUnblockFriend);
friendRoutes.put(`/decline/:id`,isLoggedInAPI, friendController.declineFriend);
friendRoutes.put(`/accept/:id`,isLoggedInAPI, friendController.acceptFriend);


friendRoutes.get('/profile/:id', isLoggedInAPI, friendController.getProfile);
friendRoutes.get('/newProfile',isLoggedInAPI, friendController.getNewProfile);
friendRoutes.get('/relationship/:id',isLoggedInAPI, friendController.getAllRelationship);