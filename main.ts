import express, { Request, Response, NextFunction } from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
import Knex from 'knex';
const grant = require('grant-express')
import path from 'path';
import multer from 'multer';
import dotenv from 'dotenv';
import {isLoggedIn } from './guards';

dotenv.config();

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

const app = express();

app.use(bodyParser.urlencoded({ extended: true })); // 普通form submit
app.use(bodyParser.json()); // json content

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})

export const upload = multer({ storage })

// ---------------------------- testing ----------------------------

export const uploadTemp = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, `${__dirname}`);
        },
        filename: function (req, file, cb) {
            cb(null, `temp.wav`);
        }
    })
})


// ----------------------------End of testing----------------------------

const sessionMiddleware = expressSession({
    secret: 'treehole secret secret typescript',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
});

app.use(sessionMiddleware);

// for google login

app.use(grant({
    "defaults": {
        "protocol": "http",
        "host": process.env.HOST || "localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile", "email"],
        "callback": "/login/google"
    },
}));
// ----------newly add MVC---------------

import {UserService} from './Service/userService';
import {UserController} from './Controller/userController';

export const userService = new UserService(knex);
export const userController = new UserController(userService);

import {MessageService} from './Service/messageService';
import {MessageController} from './Controller/messageController';
export const messageService = new MessageService(knex);
export const messageController = new MessageController(messageService);

import {FriendService} from './Service/friendService';
import {FriendController} from './Controller/friendController';
export const friendService = new FriendService(knex);
export const friendController = new FriendController(friendService);


import {userRoutes} from './routes';
import {messageRoutes} from './routes';
import {friendRoutes} from './routes';

app.use('/',userRoutes);
app.use('/',messageRoutes);
app.use('/',friendRoutes);

// ----------End of MVC----------

app.use(express.static('public'))

app.use('/uploads', isLoggedIn, express.static(`${__dirname}` + '/uploads'))

app.use(function (req: Request, res: Response, next: NextFunction) {
    if (req.session) {
        if (!req.session.counter) {
            req.session.counter = 1;
        } else {
            req.session.counter += 1;
        }
    }
    next();
});

// -------------------Login Logic-------------------------
app.get('/current-user', function (req, res) {
    if (req.session && req.session.user) {
        res.json(req.session.user);
    } else {
        res.json({ message: "Not logged in" });
    }
});


// app.post('/audioTemp', isLoggedInAPI, uploadTemp.single('audio'), async function (req, res) {
//     try {
//         if (req.session && req.session.user) {
//             res.json("success : true")
//         }
//     } catch (e) {
//         console.log(e)
//         res.status(500).json("internal server error")
//     }
// })

const PORT = 8080;

app.use(function (req, res) {
    res.sendFile(path.join(__dirname, "public/html/404.html"))
})

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`)
});

export function randomProPic() {
    let proPic = ['cat', 'dog', 'frog', 'giraffe', 'hippo', 'lion', 'monkey', 'pig', 'sheep', 'tiger']
    let luckyDraw = Math.floor(Math.random() * proPic.length)
    switch (luckyDraw) {
        case 0:
            return proPic[0];
        case 1:
            return proPic[1];
        case 2:
            return proPic[2];
        case 3:
            return proPic[3];
        case 4:
            return proPic[4];
        case 5:
            return proPic[5];
        case 6:
            return proPic[6];
        case 7:
            return proPic[7];
        case 8:
            return proPic[8];
        case 9:
            return proPic[9];
        default:
            return proPic[0]
    }
}