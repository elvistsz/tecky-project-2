import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('users')){
        return
    } 
    await knex.schema.createTable('users',(table)=>{
        table.increments();
        table.string('username');
        table.string('password');
        table.string('nickname');
        table.string('profile_pic');
        table.string('email');
        table.timestamps(false,true);
    });
    
    if(await knex.schema.hasTable('voice_message')){
        return
    } 
    await knex.schema.createTable('voice_message',(table)=>{
        table.increments();
        table.integer('users_id').unsigned();
        table.foreign('users_id').references('users.id');
        table.string('message');
        table.boolean('hidden');
        table.string('emoji')
        table.timestamps(false,true);
    });

    if(await knex.schema.hasTable('relationship')){
        return
    } 
    await knex.schema.createTable('relationship',(table)=>{
        table.increments();
        table.integer('users_one_id').unsigned();
        table.foreign('users_one_id').references('users.id');
        table.integer('users_two_id').unsigned();
        table.foreign('users_two_id').references('users.id');
        table.integer('status');
        table.integer('action_users_id');
        table.timestamps(false,true)
    });

    if(await knex.schema.hasTable('comment')){
        return
    } 
    await knex.schema.createTable('comment',(table)=>{
        table.increments();
        table.integer('message_id').unsigned();
        table.foreign('message_id').references('voice_message.id')
        table.integer('users_id').unsigned();
        table.foreign('users_id').references('users.id')
        table.string('comment');
        table.boolean('hidden');
        table.timestamps(false,true)
    });
    
    if(await knex.schema.hasTable('like_or_dislike')){
        return
    } 
    await knex.schema.createTable('like_or_dislike',(table)=>{
        table.increments();
        table.integer('message_id').unsigned();
        table.foreign('message_id').references('voice_message.id')
        table.integer('users_id').unsigned();
        table.foreign('users_id').references('users.id')
        table.boolean('like_or_dislike');
        table.timestamps(false,true);
    });
};

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("like_or_dislike")
    await knex.schema.dropTableIfExists("comment")
    await knex.schema.dropTableIfExists("relationship")
    await knex.schema.dropTableIfExists("voice_message")
    await knex.schema.dropTableIfExists("users")
}

