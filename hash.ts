import bcrypt from 'bcryptjs'

const SALT_ROUNDS = 10;

export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};

export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}

// // 同一個password 每一次hash ，都因為salt 唔同，而得到唔同結果
// hashPassword('admin')
//     .then(console.log);

// hashPassword('elvis')
// .then(console.log);

// hashPassword('zak')
// .then(console.log);

// hashPassword('bryan')
// .then(console.log);
